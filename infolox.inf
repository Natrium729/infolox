!% -Cu
!% -G
!% ++include_path=./infolox
!% $OMIT_UNUSED_ROUTINES=1

! Options

! Uncomment the following line to trace Infolox execution.
! Constant INFOLOX_TRACE_EXECUTION;

! Uncomment the following line to disassemble the code after compilation.
! Constant INFOLOX_PRINT_CODE;

! Uncomment the following line to trigger the GC as often as possible.
! Constant INFOLOX_DEBUG_STRESS_GC;

! Uncomment the following line to log the GC's inner working.
! Constant INFOLOX_DEBUG_LOG_GC;

! Uncomment this line to compile Infolox with tests.
! And then type ":test" in-game to launch them.
! Constant INFOLOX_INCLUDE_TESTS;


! Things required by Inform's veneer, which are usually defined by the standard library (which we don't use).
Default debug_flag = 0;
#Ifdef debug_flag; #Endif;
Ifndef infix__watching;
Attribute infix__watching;
#Endif;
#Ifdef infix__watching; #Endif;

! To manage the Glk windows at the start.
Global gg_mainwin = 0;
Constant GG_MAINWIN_ROCK = 200;
Constant gestalt_Unicode = 15;
Constant gestalt_DateTime = 20;
[ GGInitialise res;
	! Test if the interpreter has Glk.
	@gestalt 4 2 res;
	if (res == 0) quit;

	! Set the VM's I/O system to Glk.
	@setiosys 2 0;

	! No need to recover Glk objects since there'll never be restart or save/restore.

	! Open a buffer window.
	gg_mainwin = glk($0023, 0, 0, 0, 3, GG_MAINWIN_ROCK); ! window_open
	if (gg_mainwin == 0) quit;
	glk($002F, gg_mainwin); ! set_window

	! Check Unicode capabilities.
	@gestalt 5 0 res;
	if (res == 0) {
		print "Infolox require an interpreter with Unicode support. Exiting...";
		quit;
	}
	res = glk($4, gestalt_Unicode, 0); ! glk_gestalt;
	if (res == 0) {
		print "Infolox require an interpreter with Glk Unicode support. Exiting...";
		quit;
	}

	! Check for mcopy support.
	@gestalt 6 0 res;
	if (res == 0) {
		print "Infolox require an interpreter with MemCopy support. Exiting...";
		quit;
	}

	! Check for malloc support.
	@gestalt 7 0 res;
	if (res == 0) {
		print "Infolox require an interpreter with MAlloc support. Exiting...";
		quit;
	}

	! Check for floating-point number support.
	@gestalt 11 0 res;
	if (res == 0) {
		print "Infolox require an interpreter with floating-point number support. Exiting...";
		quit;
	}

	! Check for clock support.
	res = glk($4, gestalt_DateTime, 0); ! glk_gestalt;
	if (res == 0) {
		print "Infolox require an interpreter with Glk system clock support. Exiting...";
		quit;
	}
];

Constant style_Normal = 0;
Constant style_Header = 3;
[ glk_set_style _vararg_count;
	! glk_set_style(uint)
	@glk 134 _vararg_count 0;
	return 0;
];

[ PrintBanner i;
	glk_set_style(style_Header);
	print "Infolox";
	glk_set_style(style_Normal);
	print "^A Lox compiler and interpreter by Nathanaël Marion^Serial number ";
	! We don't track the release/version, the serial number should be enough.
	for (i=0 : i<6 : i++) print (char) $36->i;
	print " / Inform ";
	inversion;
	#Ifdef STRICT_MODE;
	print " S";
	#Endif;
	#Ifdef DEBUG;
	#Ifndef STRICT_MODE;
	print " ";
	#Endif;
	print "D";
	#Endif;
	new_line;
];

Constant evtype_LineInput = 3;
Constant LINE_INPUT_MAX = 256 + 1; ! Plus 1 for the null character.
Array line_input --> LINE_INPUT_MAX;
Array gg_event --> 4;


Include "infolox";


Constant NOT_A_COMMAND = 0;
Constant INVALID_COMMAND = 1;
Constant QUIT_COMMAND = 2;
Constant OPEN_COMMAND = 3;
#Ifdef INFOLOX_INCLUDE_TESTS;
Constant TEST_COMMAND = 4;
#Endif;

[ InputIsSpecialCommand len;
	if (len == 0 || line_input-->0 ~= ':') return NOT_A_COMMAND;

	switch (line_input-->1) {
		'o':
			if (len ~= 5) return INVALID_COMMAND;
			if (
				line_input-->2 == 'p'
				&& line_input-->3 == 'e'
				&& line_input-->4 == 'n'
			) return OPEN_COMMAND;
			else return INVALID_COMMAND;
		'q':
			if (len ~= 5) return INVALID_COMMAND;
			if (
				line_input-->2 == 'u'
				&& line_input-->3 == 'i'
				&& line_input-->4 == 't'
			) return QUIT_COMMAND;
			else return INVALID_COMMAND;
		#Ifdef INFOLOX_INCLUDE_TESTS;
		't':
			if (len ~= 5) return INVALID_COMMAND;
			if (
				line_input-->2 == 'e'
				&& line_input-->3 == 's'
				&& line_input-->4 == 't'
			) return TEST_COMMAND;
			else return INVALID_COMMAND;
		#Endif;
		default: return INVALID_COMMAND;
	}
];

[ InputLoop;
	while (true) {
		print ">";
		glk($141, gg_mainwin, line_input, LINE_INPUT_MAX, 0); ! glk_request_line_event_uni

		.GlkSelect;
		glk($C0, gg_event);! glk_select;
		if (gg_event-->0 == evtype_LineInput) {
			switch (InputIsSpecialCommand(gg_event-->2)) {
				OPEN_COMMAND: LoxRunFile();
				QUIT_COMMAND:
					print "Goodbye!^";
					return; ! End the loop.
				#Ifdef INFOLOX_INCLUDE_TESTS;
				TEST_COMMAND:
					#Ifdef INFOLOX_TRACE_EXECUTION;
					print "Tests cannot be run when `INFOLOX_TRACE_EXECUTION` is defined.";
					break;
					#Endif;
					#Ifdef INFOLOX_PRINT_CODE;
					print "Tests cannot be run when `INFOLOX_PRINT_CODE` is defined.^";
					break;
					#Endif;
					#Ifdef INFOLOX_DEBUG_STRESS_GC;
					print "Tests cannot be run when `INFOLOX_DEBUG_STRESS_GC` is defined.^";
					break;
					#Endif;
					#Ifdef INFOLOX_DEBUG_LOG_GC;
					print "Tests cannot be run when `INFOLOX_DEBUG_LOG_GC` is defined.^";
					break;
					#Endif;
					InfoloxTestAll();
				#Endif;
				INVALID_COMMAND:
					print "Unrecognised command. The commands are ~:open~ and ~:quit~.^";
				default:
					line_input-->(gg_event-->2) = 0; ! Add null character at the end.
					Interpret(line_input);
			}
		} else {
			jump GlkSelect; ! Ignore non-line-input events.
		}
	}
];

[ Main;
	GGInitialise();

	PrintBanner();

	print "^^Welcome to Infolox!^^For more information about Lox, see https://craftinginterpreters.com/^^Type Lox statements at the prompt to execute them.^^Type ~:open~ to choose a Lox file to execute. (Note: it's likely your Glulx interpreter will require the file to have a ~.glkdata~ extension.)^^Type ~:quit~ to exit.^^";

	#Ifdef INFOLOX_INCLUDE_TESTS;
	print "Type ~:test~ to run the tests.^^";
	#Endif;

	print "Note that opening a file";
	#Ifdef INFOLOX_INCLUDE_TESTS;
	print " or running the tests";
	#Endif;
	print " will clear the globals created in the REPL. Globals created by the file will be usable in the REPL after it has run.^^";

	InitVM();

	InputLoop();

	FreeVM();
];
