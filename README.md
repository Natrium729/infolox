# Infolox

Infolox is an implementation of the language Lox (from the book [*Crafting Interpreters*](https://craftinginterpreters.com/) by Bob Nystrom—go read it, it's great!) in Inform 6.

More specifically, it implements the bytecode interpreter from the third part of the book.

## Why Inform 6?

I chose Inform 6 for the following reasons:

- There were no implementations in Inform 6 yet in [the list of implementations](https://github.com/munificent/craftinginterpreters/wiki/Lox-implementations).
- It's a language I already knew.
- It's low-level like C, so it's easier to stay close to the book.
- It seemed like a terrible idea, and who doesn't like terrible ideas? :)

## Usage

Compile `infolox.inf` with:

```
$ inform6 infolox.inf
```

Then open the resulting file in you favourite Glulx interpreter and follow the instructions on-screen. However, if you intend to run long-running Lox scripts or the tests, I recommend using glulxe or git (the Glulx interpreter, not the version control system), that are included in e.g. Gargoyle, rather that use something like Lectrote, which is slower.

Before compiling `infolox.inf`, you can uncomment some constants at the top to enable various debugging features or to include the tests. After compiling with the tests, you can run them by typing ":test" in-game.

Infolox passes all the tests from the Lox implementation written in C (if there aren't any bug in my test runner generated with `build-test.py`), except for those involving `super` in a closure (see below).

## Differences with the original C implementation from the book

Infolox tries to follow the C implementation as closely as possible. That's why the VM is an object with property while it could have been a bunch of globals, for instance. However, differences are obviously unavoidable. Besides trivial ones (syntax, casing, etc.):

- Inform 6 has no structs (nor dynamic object creation), so instead we simulate them by allocating memory on the heap with `@malloc`.
- The functions from the C standard library are reimplemented in Inform 6 when needed.
- Inform 6 has no macros so instead either they are implemented as routines, or the macros' contents are written manually wherever they would be used, depending on the case.
- Where the C implementation uses a pointer, we most of the time store an offset `i` and use `-->i`. This is because if we stored an address, we would need, for a word array, to add `WORDSIZE` to "increment" it. It's easier to just use `++` and let the Glulx interpreter access the word number `i` with `-->` (that's possibly faster, too).
- The stack of the VM is allocated in the `InitVM` routine and freed in `FreeVM` since we can't declare an array property with a given size in Inform 6.
- Instead of storing the function pointers and precedences in a huge table (for Pratt's algorithm), we have routines that use a switch to return the correct precedence and rules.
- The type of a value in value arrays, hash tables and the VM stack is simply stored before it (since Inform 6 doesn't have structs). Also, as far as Inform 6 is concerned, everything is a number (be it a literal number or an address), so we don't need to implement the conversion macros (`NUMBER_VAL`, `AS_NUMBER`, `IS_NUMBER` and so on). Finally, since Inform 6 routines can only return one value, we can get the type of a value on the stack with `PeekType()` before using `Pop()`, which returns the value.
- Since it's not possible to start a Glulx file with command line arguments, we start directly in the REPL. The user can quit or open Lox files with special commands starting with `:` (Lox doesn't use colons so there are no conflicts).
- At runtime, integers are printed with a fixed number of decimals. So with `print 729;`, the output is "729.00000". I suppose we could detect that case, but it's simpler not to bother.

## Known bugs and possible enhancements

- Using `super` in a closure will cause an infinite loop. When an upvalue closes over it, it ends up pointing to a wrong address and the interpreter tries to get the method from this random address, which leads nowhere. Unfortunately, I couldn't find the cause of this bug.
- NaN-boxing is not implemented. I'm not even sure it's possible because Glulx uses 32-bit values, and I don't think there are enough spare bits in a 32-bit NaN to encode every possible Lox values. Moreover, the full 32-bit range is theoretically addressable in Glulx, so it wouldn't be possible to encode every addresses to a Lox object in a NaN. (Even though I doubt Lox script would allocate so much memory.)
- We could have used Glulx strings (arrays starting with 0xE2) to represent Lox strings, and then it would have been possible to use the `@streamstr` opcode, which should be more performant than looping ourselves over the characters.
- It's absolutely possible there are memory leaks.
