! The types of Lox objects.
Constant OBJ_BOUND_METHOD = 0;
Constant OBJ_CLASS = OBJ_BOUND_METHOD + 1;
Constant OBJ_CLOSURE = OBJ_CLASS + 1;
Constant OBJ_FUNCTION = OBJ_CLOSURE + 1;
Constant OBJ_INSTANCE = OBJ_FUNCTION + 1;
Constant OBJ_NATIVE = OBJ_INSTANCE + 1;
Constant OBJ_STRING = OBJ_NATIVE + 1;
Constant OBJ_UPVALUE = OBJ_STRING + 1;


! The properties of Lox objects.
! Unfortunately, they have the same naming scheme as the types above, but they don't clash.
Constant OBJ_TYPE = 0; ! Number
Constant OBJ_IS_MARKED = 1; ! Bool
Constant OBJ_NEXT = 2; ! Obj
Constant OBJ_SIZE = 3;

Constant OBJFUNCTION_ARITY = OBJ_SIZE; ! Number
Constant OBJFUNCTION_UPVALUE_COUNT = OBJFUNCTION_ARITY + 1; ! Number
Constant OBJFUNCTION_CHUNK = OBJFUNCTION_UPVALUE_COUNT + 1; ! Chunk
Constant OBJFUNCTION_NAME = OBJFUNCTION_CHUNK + 1; ! ObjString
Constant OBJFUNCTION_SIZE = OBJFUNCTION_NAME + 1;

Constant OBJNATIVE_FUNCTION = OBJ_SIZE; ! Routine
Constant OBJNATIVE_SIZE = OBJNATIVE_FUNCTION + 1;

Constant OBJSTRING_LENGTH = OBJ_SIZE; ! Number
Constant OBJSTRING_CHARS = OBJSTRING_LENGTH + 1; ! Number[]
Constant OBJSTRING_HASH = OBJSTRING_CHARS + 1; ! Number
Constant OBJSTRING_SIZE = OBJSTRING_HASH + 1;

Constant OBJUPVALUE_LOCATION = OBJ_SIZE; ! Address to (Number, Number)
! Two next properties below have to be contiguous and in this order! This is because the location of a closed upvalue will point to these properties.
Constant OBJUPVALUE_CLOSED_TYPE = OBJUPVALUE_LOCATION + 1; ! Number
Constant OBJUPVALUE_CLOSED_VALUE = OBJUPVALUE_CLOSED_TYPE + 1; ! Number
Constant OBJUPVALUE_NEXT = OBJUPVALUE_CLOSED_VALUE + 1; ! ObjUpvalue
Constant OBJUPVALUE_SIZE = OBJUPVALUE_NEXT + 1;

Constant OBJCLOSURE_FUNCTION = OBJ_SIZE; ! ObjFunction
Constant OBJCLOSURE_UPVALUES = OBJCLOSURE_FUNCTION + 1; ! ObjUpvalues[]
Constant OBJCLOSURE_UPVALUE_COUNT = OBJCLOSURE_UPVALUES + 1; ! Number
Constant OBJCLOSURE_SIZE = OBJCLOSURE_UPVALUE_COUNT + 1;

Constant OBJCLASS_NAME = OBJ_SIZE; ! ObjString
Constant OBJCLASS_METHODS = OBJCLASS_NAME + 1; ! Table
Constant OBJCLASS_SIZE = OBJCLASS_METHODS + 1;

Constant OBJINSTANCE_CLASS = OBJ_SIZE; ! ObjClass
Constant OBJINSTANCE_FIELDS = OBJINSTANCE_CLASS + 1; ! Table
Constant OBJINSTANCE_SIZE = OBJINSTANCE_FIELDS + 1;

Constant OBJBOUNDMETHOD_RECEIVER = OBJ_SIZE; ! ObjInstance
Constant OBJBOUNDMETHOD_METHOD = OBJBOUNDMETHOD_RECEIVER + 1; ! ObjClosure
Constant OBJBOUNDMETHOD_SIZE = OBJBOUNDMETHOD_METHOD + 1;

[ IsObjType val_type val type;
	return val_type == VAL_OBJ && val-->OBJ_TYPE == type;
];

[ AllocateObject size type  o;
	o = Reallocate(0, 0, size * WORDSIZE);
	o-->OBJ_TYPE = type;
	o-->OBJ_IS_MARKED = false;
	o-->OBJ_NEXT = vm.objects;
	vm.objects = o;

	#Ifdef DEBUG_LOG_GC;
	print "    ", o, "allocate ", size, " for ", type "^";
	#Endif;

	return o;
];

[ NewBoundMethod receiver m  bound;
	bound = AllocateObject(OBJBOUNDMETHOD_SIZE, OBJ_BOUND_METHOD);
	bound-->OBJBOUNDMETHOD_RECEIVER = receiver;
	bound-->OBJBOUNDMETHOD_METHOD = m;
	return bound;
];

[ NewClass n  klass;
	klass = AllocateObject(OBJCLASS_SIZE, OBJ_CLASS);
	klass-->OBJCLASS_NAME = n;
	klass-->OBJCLASS_METHODS = NewTable();
	return klass;
];

[ NewClosure f  closure uvs i;
	uvs = Allocate(WORDSIZE, f-->OBJFUNCTION_UPVALUE_COUNT);
	for (i=0 : i<f-->OBJFUNCTION_UPVALUE_COUNT : i++) {
		uvs-->i = 0;
	}

	closure = AllocateObject(OBJCLOSURE_SIZE, OBJ_CLOSURE);
	closure-->OBJCLOSURE_FUNCTION = f;
	closure-->OBJCLOSURE_UPVALUES = uvs;
	closure-->OBJCLOSURE_UPVALUE_COUNT = f-->OBJFUNCTION_UPVALUE_COUNT;
	return closure;
];

[ NewFunction  f;
	f = AllocateObject(OBJFUNCTION_SIZE, OBJ_FUNCTION);
	f-->OBJFUNCTION_ARITY = 0;
	f-->OBJFUNCTION_UPVALUE_COUNT = 0;
	f-->OBJFUNCTION_NAME = 0;
	f-->OBJFUNCTION_CHUNK = NewChunk();
	return f;
];

[ NewInstance k  inst;
	inst = AllocateObject(OBJINSTANCE_SIZE, OBJ_INSTANCE);
	inst-->OBJINSTANCE_CLASS = k;
	inst-->OBJINSTANCE_FIELDS = NewTable();
	return inst;
];

[ NewNative f  native;
	native = AllocateObject(OBJNATIVE_SIZE, OBJ_NATIVE);
	native-->OBJNATIVE_FUNCTION = f;
	return native;
];

[ TakeString c l h  interned;
	h = HashString(c, l);
	interned = TableFindString(vm.strings, c, l, h);
	if (interned ~= 0) {
		FreeArray(WORDSIZE, c, l +1);
		return interned;
	}

	return AllocateString(c, l, h);
];

[ AllocateString c l h  str;
	str = AllocateObject(OBJSTRING_SIZE, OBJ_STRING);
	str-->OBJSTRING_LENGTH = l;
	str-->OBJSTRING_CHARS = c;
	str-->OBJSTRING_HASH = h;
	Push(VAL_OBJ, str);
	TableSet(vm.strings, str, VAL_NIL, 0);
	Pop();
	return str;
];

[ HashString chars length  hash i byte;
	! We write the values in hex unlike the book because the numbers need to be unsigned.
	! (Decimal litteral in Inform are always signed.)
	hash = $811C9DC5;
	! We compute the hash bytewise and not wordwise.
	for (i=0 : i<length*WORDSIZE : i++) {
		! Inform 6 has no bitwise XOR operator so we use the opcode directly.
		byte = chars->i;
		@bitxor hash byte hash;
		hash = hash * $1000193;
	}
	return hash;
];

[ CopyString addr l  hash heap_chars word_length interned;
	hash = HashString(addr, l);
	interned = TableFindString(vm.strings, addr, l, hash);
	if (interned ~= 0) return interned;

	heap_chars = Allocate(WORDSIZE, l + 1);

	word_length = l * WORDSIZE;
	@mcopy word_length addr heap_chars;

	heap_chars-->l = 0;
	return AllocateString(heap_chars, l, hash);
];

! `slot` is the address of the value (`slot-->0` being the type and `slot-->1` the actual value).
[ NewUpvalue slot  uv;
	uv = AllocateObject(OBJUPVALUE_SIZE, OBJ_UPVALUE);
	uv-->OBJUPVALUE_CLOSED_TYPE = VAL_NIL;
	uv-->OBJUPVALUE_CLOSED_VALUE = 0;
	uv-->OBJUPVALUE_LOCATION = slot;
	uv-->OBJUPVALUE_NEXT = 0;
	return uv;
];

[ PrintFunction f;
	if (f-->OBJFUNCTION_NAME == 0) {
		print "<script>";
		return;
	}
	print "<fn ", (PrintObject) f-->OBJFUNCTION_NAME, ">";
];

[ PrintObject o  i;
	switch (o-->OBJ_TYPE) {
		OBJ_BOUND_METHOD:
			PrintFunction(o-->OBJBOUNDMETHOD_METHOD-->OBJCLOSURE_FUNCTION);
		OBJ_CLASS: PrintObject(o-->OBJCLASS_NAME);
		OBJ_CLOSURE: PrintFunction(o-->OBJCLOSURE_FUNCTION);
		OBJ_FUNCTION: PrintFunction(o);
		OBJ_INSTANCE:
			print (PrintObject) o-->OBJINSTANCE_CLASS-->OBJCLASS_NAME, " instance";
		OBJ_NATIVE: print "<native fn>";
		OBJ_STRING:
			for (i=0 : i<o-->OBJSTRING_LENGTH : i++) {
				print (char) o-->OBJSTRING_CHARS-->i;
			}
		OBJ_UPVALUE: print "upvalue";
		default: InfoloxFatalError("PrintObject", "unreachable branch");
	}
];
