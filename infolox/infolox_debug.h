[ DisassembleChunk chunk n  offset;
	print "== ";
	if (metaclass(n) == String) {
		print (string) n;
	} else {
		for (offset=0 : n-->offset~=0 : offset++) {
			print (char) n-->offset;
		}
	}
	print " ==^";

	for (offset=0 : offset<chunk-->CHUNK_COUNT :) {
		offset = DisassembleInstruction(chunk, offset);
	}
];

[ ConstantInstruction n chunk offset  const;
	const = chunk-->CHUNK_CODE->(offset + 1);
	print (string) n, " ", const, " '";
	PrintValue(
		chunk-->CHUNK_CONSTANTS-->VALUE_ARRAY_VALUES-->(VALUE_SIZE * const),
		chunk-->CHUNK_CONSTANTS-->VALUE_ARRAY_VALUES-->(VALUE_SIZE * const + 1)
	);
	print "'^";
	return offset + 2;
];

[ InvokeInstruction n chunk offset  const arg_count;
	const = chunk-->CHUNK_CODE->(offset + 1);
	arg_count = chunk-->CHUNK_CODE->(offset + 2);
	print (string) n, " (", arg_count, " args) ", const, " '";
	PrintValue(
		chunk-->CHUNK_CONSTANTS-->VALUE_ARRAY_VALUES-->(VALUE_SIZE * const),
		chunk-->CHUNK_CONSTANTS-->VALUE_ARRAY_VALUES-->(VALUE_SIZE * const + 1)
	);
	print "'^";
	return offset + 3;
];

[ SimpleInstruction n offset;
	print (string) n, "^";
	return offset + 1;
];

[ ByteInstruction n chunk offset  slot;
	slot = chunk-->CHUNK_CODE->(offset + 1);
	print (string) n, " ", slot, "^";
	return offset + 2;
];

[ JumpInstruction n sign chunk offset  jmp;
	jmp = chunk-->CHUNK_CODE->(offset + 1);
	@shiftl jmp 8 jmp;
	jmp = jmp | (chunk-->CHUNK_CODE->(offset + 2));
	print (string) n, " ", offset, " -> ", offset + 3 + sign * jmp, "^";
	return offset + 3;
];

[ DisassembleInstruction chunk offset  instruction const f j;
	print offset, " ";
	if (offset > 0 && chunk-->CHUNK_LINES-->offset == chunk-->CHUNK_LINES-->(offset - 1)) {
		print "|";
	} else {
		print chunk-->CHUNK_LINES-->offset;
	}
	print " ";

	instruction = chunk-->CHUNK_CODE->offset;
	switch (instruction) {
		OP_CONSTANT: return ConstantInstruction("OP_CONSTANT", chunk, offset);
		OP_NIL: return SimpleInstruction("OP_NIL", offset);
		OP_TRUE: return SimpleInstruction("OP_TRUE", offset);
		OP_FALSE: return SimpleInstruction("OP_FALSE", offset);
		OP_POP: return SimpleInstruction("OP_POP", offset);
		OP_GET_LOCAL: return ByteInstruction("OP_GET_LOCAL", chunk, offset);
		OP_SET_LOCAL: return ByteInstruction("OP_SET_LOCAL", chunk, offset);
		OP_GET_GLOBAL: return ConstantInstruction("OP_GET_GLOBAL", chunk, offset);
		OP_DEFINE_GLOBAL: return ConstantInstruction("OP_DEFINE_GLOBAL", chunk, offset);
		OP_SET_GLOBAL: return ConstantInstruction("OP_SET_GLOBAL", chunk, offset);
		OP_GET_UPVALUE: return ByteInstruction("OP_GET_UPVALUE", chunk, offset);
		OP_SET_UPVALUE: return ByteInstruction("OP_SET_UPVALUE", chunk, offset);
		OP_GET_PROPERTY: return ConstantInstruction("OP_GET_PROPERTY", chunk, offset);
		OP_SET_PROPERTY: return ConstantInstruction("OP_SET_PROPERTY", chunk, offset);
		OP_GET_SUPER: return ConstantInstruction("OP_GET_SUPER", chunk, offset);
		OP_EQUAL: return SimpleInstruction("OP_EQUAL", offset);
		OP_GREATER: return SimpleInstruction("OP_GREATER", offset);
		OP_LESS: return SimpleInstruction("OP_LESS", offset);
		OP_ADD: return SimpleInstruction("OP_ADD", offset);
		OP_SUBTRACT: return SimpleInstruction("OP_SUBTRACT", offset);
		OP_MULTIPLY: return SimpleInstruction("OP_MULTIPLY", offset);
		OP_DIVIDE: return SimpleInstruction("OP_DIVIDE", offset);
		OP_NOT: return SimpleInstruction("OP_NOT", offset);
		OP_NEGATE: return SimpleInstruction("OP_NEGATE", offset);
		OP_PRINT: return SimpleInstruction("OP_PRINT", offset);
		OP_JUMP: return JumpInstruction("OP_JUMP", 1, chunk, offset);
		OP_JUMP_IF_FALSE: return JumpInstruction("OP_JUMP_IF_FALSE", 1, chunk, offset);
		OP_LOOP: return JumpInstruction("OP_LOOP", -1, chunk, offset);
		OP_CALL: return ByteInstruction("OP_CALL", chunk, offset);
		OP_INVOKE: return InvokeInstruction("OP_INVOKE", chunk, offset);
		OP_SUPER_INVOKE: return InvokeInstruction("OP_SUPER_INVOKE", chunk, offset);
		OP_CLOSURE:
			offset++;
			const = chunk-->CHUNK_CODE->(offset++);
			print "OP_CLOSURE ", const, " ";
			PrintValue(
				chunk-->CHUNK_CONSTANTS-->VALUE_ARRAY_VALUES-->(VALUE_SIZE * const),
				chunk-->CHUNK_CONSTANTS-->VALUE_ARRAY_VALUES-->(VALUE_SIZE * const + 1)
			);
			new_line;

			f = chunk-->CHUNK_CONSTANTS-->VALUE_ARRAY_VALUES-->(VALUE_SIZE * const + 1);
			for (j=0 : j<f-->OBJFUNCTION_UPVALUE_COUNT : j++) {
				print offset - 2, " |   ";
				if (chunk-->CHUNK_CODE->(offset++)) {
					print "local ";
				} else {
					print "upvalue ";
				}
				print chunk-->CHUNK_CODE->(offset++), "^";
			}

			return offset;
		OP_CLOSE_UPVALUE: return SimpleInstruction("OP_CLOSE_UPVALUE", offset);
		OP_RETURN: return SimpleInstruction("OP_RETURN", offset);
		OP_CLASS: return ConstantInstruction("OP_CLASS", chunk, offset);
		OP_INHERIT: return SimpleInstruction("OP_INHERIT", offset);
		OP_METHOD: return ConstantInstruction("OP_METHOD", chunk, offset);
		default:
			print "Unknown opcode ", instruction, "^";
			return offset + 1;
	}
];
