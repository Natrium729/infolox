! The list of possible tokens.

Constant TOKEN_LEFT_PAREN = 0;
Constant TOKEN_RIGHT_PAREN = 1;
Constant TOKEN_LEFT_BRACE = 2;
Constant TOKEN_RIGHT_BRACE = 3;
Constant TOKEN_COMMA = 4;
Constant TOKEN_DOT = 5;
Constant TOKEN_MINUS = 6;
Constant TOKEN_PLUS = 7;
Constant TOKEN_SEMICOLON = 8;
Constant TOKEN_SLASH = 9;
Constant TOKEN_STAR = 10;
! One or two character tokens.
Constant TOKEN_BANG = 11;
Constant TOKEN_BANG_EQUAL = 12;
Constant TOKEN_EQUAL = 13;
Constant TOKEN_EQUAL_EQUAL = 14;
Constant TOKEN_GREATER = 15;
Constant TOKEN_GREATER_EQUAL = 16;
Constant TOKEN_LESS = 17;
Constant TOKEN_LESS_EQUAL = 18;
! Literals.
Constant TOKEN_IDENTIFIER = 19;
Constant TOKEN_STRING = 20;
Constant TOKEN_NUMBER = 21;
! Keywords.
Constant TOKEN_AND = 22;
Constant TOKEN_CLASS = 23;
Constant TOKEN_ELSE = 24;
Constant TOKEN_FALSE = 25;
Constant TOKEN_FOR = 26;
Constant TOKEN_FUN = 27;
Constant TOKEN_IF = 28;
Constant TOKEN_NIL = 29;
Constant TOKEN_OR = 30;
Constant TOKEN_PRINT = 31;
Constant TOKEN_RETURN = 32;
Constant TOKEN_SUPER = 33;
Constant TOKEN_THIS = 34;
Constant TOKEN_TRUE = 35;
Constant TOKEN_VAR = 36;
Constant TOKEN_WHILE = 37;

Constant TOKEN_ERROR = 38;
Constant TOKEN_EOF = 39;


Object scanner with
	source 0,
	start 0,
	current 0,
	line 0,
;

! The properties of tokens. Unfortunaltely, the naming scheme is the same as the token types above, but there are no clashes.
Constant TOKEN_TYPE = 0; ! Number
Constant TOKEN_START = 1; ! Number
Constant TOKEN_LENGTH = 2; ! Number
Constant TOKEN_LINE = 3; ! Number
Constant TOKEN_NEXT = 4; ! Token
Constant TOKEN_SIZE = 5;

! The linked list containing all tokens, so that we can free them after compiling a Lox script.
Global token_list = 0;

[ MakeToken t  tok;
	tok = TOKEN_SIZE * WORDSIZE;
	@malloc tok tok;
	if (tok == 0) InfoloxFatalError("MakeToken", "couldn't allocate memory");
	tok-->TOKEN_TYPE = t;
	tok-->TOKEN_START = scanner.start;
	tok-->TOKEN_LENGTH = scanner.current - scanner.start;
	tok-->TOKEN_LINE = scanner.line;
	tok-->TOKEN_NEXT = token_list;
	token_list = tok;
	return tok;
];

[ ErrorToken msg  tok;
	tok = MakeToken(TOKEN_ERROR);
	tok-->TOKEN_START = msg;
	tok-->TOKEN_LENGTH = 0;
	return tok;
];

[ FreeTokens  tok next;
	tok = token_list;
	while (tok ~= 0) {
		next = tok-->TOKEN_NEXT;
		@mfree tok;
		tok = next;
	}
	token_list = 0;
];

[ SkipWhitespace;
	while (true) {
		switch (ScannerPeek()) {
			' ', 9, 13: ScannerAdvance(); ! Space, \r, \t.
			10: ! \n
				scanner.line++;
				ScannerAdvance();
			'/': if (PeekNext() == '/') {
				! A comment goes until the end of the line.
				while (ScannerPeek() ~= 10 && (~~IsAtEnd())) ScannerAdvance();
			} else {
				return;
			}
			default: return;
		}
	}
];

[ CheckKeyword _vararg_count  checked_start i c;
		_vararg_count = _vararg_count - 2; ! Minus 2 for the first and last argument.
		@pull checked_start;
		if (scanner.current - scanner.start ~= checked_start + _vararg_count) return TOKEN_IDENTIFIER;

		for (
			i = scanner.start + checked_start :
			i < scanner.start + checked_start + _vararg_count :
			i++
		) {
			@pull c;
			if (c ~= scanner.source-->i) return TOKEN_IDENTIFIER;
		}

		@return sp;
];

[ IdentifierType;
	switch (scanner.source-->scanner.start) {
		'a': return CheckKeyword(1, 'n', 'd', TOKEN_AND);
		'c': return CheckKeyword(1, 'l', 'a', 's', 's', TOKEN_CLASS);
		'e': return CheckKeyword(1, 'l', 's', 'e', TOKEN_ELSE);
		'f': if (scanner.current - scanner.start > 1) {
			switch (scanner.source-->(scanner.start+1)) {
				'a': return CheckKeyword(2, 'l', 's', 'e', TOKEN_FALSE);
				'o': return CheckKeyword(2, 'r', TOKEN_FOR);
				'u': return CheckKeyword(2, 'n', TOKEN_FUN);
			}
		}
		'i': return CheckKeyword(1, 'f', TOKEN_IF);
		'n': return CheckKeyword(1, 'i', 'l', TOKEN_NIL);
		'o': return CheckKeyword(1, 'r', TOKEN_OR);
		'p': return CheckKeyword(1, 'r', 'i', 'n', 't', TOKEN_PRINT);
		'r': return CheckKeyword(1, 'e', 't', 'u', 'r', 'n', TOKEN_RETURN);
		's': return CheckKeyword(1, 'u', 'p', 'e', 'r', TOKEN_SUPER);
		't': if (scanner.current - scanner.start > 1) {
			switch (scanner.source-->(scanner.start+1)) {
				'h': return CheckKeyword(2, 'i', 's', TOKEN_THIS);
				'r': return CheckKeyword(2, 'u', 'e', TOKEN_TRUE);
			}
		}
		'v': return CheckKeyword(1, 'a', 'r', TOKEN_VAR);
		'w': return CheckKeyword(1, 'h', 'i', 'l', 'e', TOKEN_WHILE);
	}
	return TOKEN_IDENTIFIER;
];

[ Identifier;
	while (IsAlpha(ScannerPeek()) || IsDigit(ScannerPeek())) ScannerAdvance();
	return MakeToken(IdentifierType());
];

[ ScanNumber;
	while (IsDigit(ScannerPeek())) ScannerAdvance();

	! Look for a fractional part.
	if (ScannerPeek() == '.' && IsDigit(PeekNext())) {
		! Consume the dot.
		ScannerAdvance();

		while (IsDigit(ScannerPeek())) ScannerAdvance();
	}

	return MakeToken(TOKEN_NUMBER);
];

[ ScanString;
	while (ScannerPeek() ~= '"' && (~~IsAtEnd())) {
		if (ScannerPeek() == 10) scanner.line++;
		ScannerAdvance();
	}

	if (IsAtEnd()) return ErrorToken("Unterminated string.");

	! The closing quote.
	ScannerAdvance();
	return MakeToken(TOKEN_STRING);
];

[ InitScanner source_arr;
	! Unlike clox, `scanner.start` and `scanner.current` are offsets and not addresses,
	! so we need to store the address of the source.
	scanner.source = source_arr;
	scanner.start = 0;
	scanner.current = 0;
	scanner.line = 1;
];

[ IsAlpha c;
	return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || c == '_';
];

[ IsDigit c;
	return c >= '0' && c <= '9';
];

[ IsAtEnd;
	return scanner.source-->scanner.current == 0;
];

[ ScannerAdvance;
	return scanner.source-->(scanner.current++);
];

[ ScannerPeek;
	return scanner.source-->scanner.current;
];

[ PeekNext;
	if (IsAtEnd()) return 0;
	return scanner.source-->(scanner.current + 1);
];

[ ScannerMatch expected;
	if (IsAtEnd()) rfalse;
	if (scanner.source-->scanner.current ~= expected) rfalse;
	scanner.current++;
	rtrue;
];

[ ScanToken  c;
	SkipWhitespace();
	scanner.start = scanner.current;

	if (IsAtEnd()) return MakeToken(TOKEN_EOF);

	c = ScannerAdvance();
	if (IsAlpha(c)) return Identifier();
	if (IsDigit(c)) return ScanNumber();

	switch (c) {
		'(': return MakeToken(TOKEN_LEFT_PAREN);
		')': return MakeToken(TOKEN_RIGHT_PAREN);
		'{': return MakeToken(TOKEN_LEFT_BRACE);
		'}': return MakeToken(TOKEN_RIGHT_BRACE);
		';': return MakeToken(TOKEN_SEMICOLON);
		',': return MakeToken(TOKEN_COMMA);
		'.': return MakeToken(TOKEN_DOT);
		'-': return MakeToken(TOKEN_MINUS);
		'+': return MakeToken(TOKEN_PLUS);
		'/': return MakeToken(TOKEN_SLASH);
		'*': return MakeToken(TOKEN_STAR);
		'!':
			if (ScannerMatch('=')) return MakeToken(TOKEN_BANG_EQUAL);
			else return MakeToken(TOKEN_BANG);
		'=':
			if (ScannerMatch('=')) return MakeToken(TOKEN_EQUAL_EQUAL);
			else return MakeToken(TOKEN_EQUAL);
		'<':
			if (ScannerMatch('=')) return MakeToken(TOKEN_LESS_EQUAL);
			else return MakeToken(TOKEN_LESS);
		'>':
			if (ScannerMatch('=')) return MakeToken(TOKEN_GREATER_EQUAL);
			else return MakeToken(TOKEN_GREATER);
		'"': return ScanString();
	}

	return ErrorToken("Unexpected character.");
];
