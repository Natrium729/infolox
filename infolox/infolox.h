[ InfoloxFatalError loc msg;
	! During tests, the output is redirected to memory, so with set it back to the main window so that the message is visible.
	glk($002F, gg_mainwin); ! set_window
	print "^^Infolox fatal error (in `", (string) loc, "`): ", (string) msg, ".^";
	quit;
];

Default MAX_LOCALS = 256;

Include "infolox_memory";
Include "infolox_chunk";
Include "infolox_value";
Include "infolox_table";
Include "infolox_object";
Include "infolox_vm";
Include "infolox_compiler";
Include "infolox_scanner";
Include "infolox_debug";
Include "infolox_test";

Constant filemode_Read = 2;
Constant fileusage_TextMode = 256;
Global open_file_max_len = 0;
Global open_file_content_addr = 0;
Constant seekmode_Start = 0;

[ LoxRunFile  fref stream len len_words;
	! Ask the user to select a file and create file ref.
	! glk_fileref_create_by_prompt
	fref = glk($62, fileusage_TextMode, filemode_Read, 0);

	! If the user canceled the open dialog.
	if (~~fref) {
		print "[No file selected.]^";
		return;
	}

	! Open the file to get its stream.
	! glk_stream_open_file_uni
	stream = glk($138, fref, filemode_Read, 0);

	! Now that we have the stream, we can release the file ref.
	! glk_fileref_destroy
	glk($63, fref);

	len = 1; ! To take account of the null byte.
	! Count the number of characters in the file.
	!glk_get_char_stream_uni
	while (glk($130, stream) ~= -1) {
		len++;
	}

	! If there are more characters in the file than the array can hold.
	if (len > open_file_max_len) {
		open_file_max_len = len;
		! Free the current array and allocate a bigger one.
		if (open_file_content_addr ~= 0) {
			@mfree open_file_content_addr;
		}
		len_words = len * WORDSIZE;
		@malloc len_words open_file_content_addr;
		if (open_file_content_addr == 0) InfoloxFatalError("LoxRunFile", "couldn't allocate memory");
	}

	! Go back to the start of the file.
	! glk_stream_set_position
	glk($45, stream, 0, seekmode_Start);

	! Read the file into the dedicated array.
	! glk_get_buffer_stream_uni
	glk($131, stream, open_file_content_addr, open_file_max_len);

	! Close the file stream.
	! glk_stream_close
	glk($44, stream, -1);

	! Add null byte.
	open_file_content_addr-->(len - 1) = 0;

	! Free the VM from the REPL and init a new one.
	FreeVM();
	InitVM();

	! Run the file.
	Interpret(open_file_content_addr);

	! We don't free the new VM so that the globals from the script remain usable in the REPL.
];
