Constant GC_HEAP_GROW_FACTOR = 2;

[ Allocate size count;
	return Reallocate(0, 0, size * count);
];

[ GrowCapacity capacity;
	if (capacity < 8) {
		return 8;
	} else {
		return capacity * 2;
	}
];

[ GrowArray size addr old_count new_count;
	return Reallocate(addr, size * old_count, size * new_count);
];

[ FreeArray size addr old_count;
	return Reallocate(addr, size * old_count, 0);
];

[ Reallocate addr old_size new_size  new_addr upper_i;
	vm.bytes_allocated = vm.bytes_allocated + new_size - old_size;
	#Ifdef INFOLOX_DEBUG_STRESS_GC;
	if (new_size > old_size) {
		CollectGarbage();
	}
	#Endif;

	if (vm.bytes_allocated > vm.next_gc) {
		CollectGarbage();
	}

	if (new_size == 0) {
		if (addr) @mfree addr;
		return 0;
	}

	! Allocate a new block.
	if (old_size == 0) {
		@malloc new_size new_addr;
		if (new_addr == 0) InfoloxFatalError("Reallocate", "couldn't allocate memory");
		return new_addr;
	}

	! Do nothing.
	if (old_size == new_size) {
		return addr;
	}

	! Reallocate.
	if (old_size < new_size) {
		! To a bigger block.
		upper_i = old_size;
	} else { ! At this point the only remaining option is old_size > new_size.
		! To a smaller block.
		upper_i = new_size;
	}
	! Copy data to new address.
	@malloc new_size new_addr;
	if (new_addr == 0) InfoloxFatalError("Reallocate", "couldn't reallocate memory");
	@mcopy upper_i addr new_addr;
	! Free old address.
	@mfree addr;

	return new_addr;
];

[ MarkObject o  new_grays old_grays len;
	if (o == 0) return;
	if (o-->OBJ_IS_MARKED) return;

	#Ifdef INFOLOX_DEBUG_LOG_GC;
	print "    ", o, " mark ", (PrintObject) o, " (", (name) o, ")^";
	#Endif;

	o-->OBJ_IS_MARKED = true;

	if (vm.gray_capacity < vm.gray_count + 1) {
		! Grow capacity.
		vm.gray_capacity = GrowCapacity(vm.gray_capacity);
		! Allocate new stack.
		len = vm.gray_capacity * WORDSIZE;
		@malloc len new_grays;
		if (new_grays == 0) InfoloxFatalError("MarkObject", "couldn't allocate memory");
		! Copy values from old stack to new one.
		old_grays = vm.gray_stack;
		@mcopy len old_grays new_grays;
		if (old_grays) {
			@mfree old_grays;
		}
		! Make VM point to new one.
		vm.gray_stack = new_grays;
	}
	vm.gray_stack-->(vm.gray_count++) = o;
];

[ TraceReferences  o;
	while (vm.gray_count > 0) {
		#Ifdef INFOLOX_DEBUG_LOG_GC;
		print "    grays [";
		for (o=0 : o<vm.gray_count : o++) {
			print vm.gray_stack-->o, " ", (PrintObject) vm.gray_stack-->o;
			if (o ~= vm.gray_count - 1) print ", ";
		}
		print "]^";
		#Endif;
		o = vm.gray_stack-->(--vm.gray_count);
		BlackenObject(o);
	}
];

[ Sweep  previous o unreached;
	previous = 0;
	o = vm.objects;
	while (o ~= 0) {
		if (o-->OBJ_IS_MARKED) {
			o-->OBJ_IS_MARKED = false;
			previous = o;
			o = o-->OBJ_NEXT;
		} else {
			unreached = o;
			o = o-->OBJ_NEXT;
			if (previous ~= 0) {
				previous-->OBJ_NEXT = o;
			} else {
				vm.objects = o;
			}
			FreeObject(unreached);
		}
	}
];

[ MarkValue val_type value;
	if (val_type == VAL_OBJ) MarkObject(value);
];

[ MarkArray arr  i;
	for (i=0 : i<arr-->VALUE_ARRAY_COUNT : i++) {
		MarkValue(
			arr-->VALUE_ARRAY_VALUES-->(i * VALUE_SIZE),
			arr-->VALUE_ARRAY_VALUES-->(i * VALUE_SIZE + 1)
		);
	}
];

[ BlackenObject o  i;
	#Ifdef INFOLOX_DEBUG_LOG_GC;
	print "    ", o, " blacken ", (PrintObject) o, "^";
	#Endif;

	switch (o-->OBJ_TYPE) {
		OBJ_BOUND_METHOD:
			MarkObject(o-->OBJBOUNDMETHOD_RECEIVER);
			MarkObject(o-->OBJBOUNDMETHOD_METHOD);
		OBJ_CLASS:
			MarkObject(o-->OBJCLASS_NAME);
			MarkTable(o-->OBJCLASS_METHODS);
		OBJ_CLOSURE:
			MarkObject(o-->OBJCLOSURE_FUNCTION);
			for (i=0 : i<o-->OBJCLOSURE_UPVALUE_COUNT : i++) {
				MarkObject(o-->OBJCLOSURE_UPVALUES-->i);
			}
		OBJ_FUNCTION:
			MarkObject(o-->OBJFUNCTION_NAME);
			MarkArray(o-->OBJFUNCTION_CHUNK-->CHUNK_CONSTANTS);
		OBJ_INSTANCE:
			MarkObject(o-->OBJINSTANCE_CLASS);
			MarkTable(o-->OBJINSTANCE_FIELDS);
		OBJ_UPVALUE:
			MarkValue(o-->OBJUPVALUE_CLOSED_TYPE, o-->OBJUPVALUE_CLOSED_VALUE);
		default: ; ! Do nothing.
	}
];

[ FreeObject o;
	#Ifdef INFOLOX_DEBUG_LOG_GC;
	print "    ", o, " free type ", o-->OBJ_TYPE, "^";
	#Endif;

	switch (o-->OBJ_TYPE) {
		OBJ_BOUND_METHOD:
			Reallocate(o, OBJBOUNDMETHOD_SIZE * WORDSIZE, 0);
		OBJ_CLASS:
			FreeTable(o-->OBJCLASS_METHODS);
			Reallocate(o, OBJCLASS_SIZE * WORDSIZE, 0);
		OBJ_CLOSURE:
			FreeArray(
				WORDSIZE,
				o-->OBJCLOSURE_UPVALUES,
				o-->OBJCLOSURE_UPVALUE_COUNT
			);
			Reallocate(o, OBJCLOSURE_SIZE * WORDSIZE, 0);
		OBJ_FUNCTION:
			FreeChunk(o-->OBJFUNCTION_CHUNK);
			Reallocate(o, OBJFUNCTION_SIZE * WORDSIZE, 0);
		OBJ_INSTANCE:
			FreeTable(o-->OBJINSTANCE_FIELDS);
			Reallocate(o, OBJINSTANCE_SIZE * WORDSIZE, 0);
		OBJ_NATIVE:
			Reallocate(o, OBJNATIVE_SIZE * WORDSIZE, 0);
		OBJ_STRING:
			FreeArray(
				WORDSIZE,
				o-->OBJSTRING_CHARS,
				o-->OBJSTRING_LENGTH + 1
			);
			Reallocate(o, OBJSTRING_SIZE * WORDSIZE, 0);
		OBJ_UPVALUE:
			Reallocate(o, OBJUPVALUE_SIZE * WORDSIZE, 0);
		default: InfoloxFatalError("Freeobject", "unreachable branch");
	}
];

#Ifdef INFOLOX_DEBUG_LOG_GC;
! It could be a local of `CollectGarbage`, but having a global makes it possible to compile it conditionnaly.
! (So we don't get the unused variable warning.)
Global allocated_before = 0;
#Endif;

[ CollectGarbage;
	#Ifdef INFOLOX_DEBUG_LOG_GC;
	print "-- gc begin^";
	allocated_before = vm.bytes_allocated;
	#Endif;

	MarkRoots();
	TraceReferences();
	TableRemoveWhite(vm.strings);
	Sweep();

	vm.next_gc = vm.bytes_allocated * GC_HEAP_GROW_FACTOR;

	#Ifdef INFOLOX_DEBUG_LOG_GC;
	print "-- gc end^    collected ", allocated_before - vm.bytes_allocated, " (from ", allocated_before, " to ", vm.bytes_allocated, ") next at ", vm.next_gc, "^";
	#Endif;
];

[ FreeObjects  o next;
	o = vm.objects;
	while (o) {
		next = o-->OBJ_NEXT;
		FreeObject(o);
		o = next;
	}

	! When we call the routine from `FreeVM` and not during garbage collection, the gray stack is not allocated, hence the check.
	if (vm.gray_stack) {
		o = vm.gray_stack;
		@mfree o;
	}
];

[ MarkRoots  i;
	for (i=0 : i<vm.stack_top : i++) {
		MarkValue(
			vm.stack-->(i * VALUE_SIZE),
			vm.stack-->(i * VALUE_SIZE + 1)
		);
	}

	for (i=0 : i<vm.frame_count : i++) {
		MarkObject((vm.frames-->i).closure);
	}

	for (i=vm.open_upvalues : i ~= 0 : i = i-->OBJUPVALUE_NEXT) {
		MarkObject(i);
	}

	MarkTable(vm.globals);
	MarkCompilerRoots();
	MarkObject(vm.init_string);
];

! Reimplementation of C's `memcmp`.
! It compares wordwise.
[ memcmp arr1 arr2 length  i;
	for (i=0 : i<length : i++) {
		if (arr1-->i ~= arr2-->i) rfalse;
	}
	rtrue;
];
