! The list of precedences.

Constant PREC_NONE = 0;
Constant PREC_ASSIGNMENT = 1; ! =
Constant PREC_OR = 2 ; ! or
Constant PREC_AND = 3; ! and
Constant PREC_EQUALITY = 4; ! == !=
Constant PREC_COMPARISON = 5; ! < > <= >=
Constant PREC_TERM = 6; ! + -
Constant PREC_FACTOR = 7; ! * /
Constant PREC_UNARY = 8; ! ! -
Constant PREC_CALL = 9; ! . ()
! Not sure what's its use. It doesn't seem to be used by anything.
! Constant PREC_PRIMARY = 10;

! Some "strings" the compiler or the VM will need.
! Used to detect the `this` token.
Array this_array table "this";
! Used to detect the initialisers of classes and to set-up `vm.init_string`.
Array init_array table "init";
! Used to make a synthetic token when inheriting a class.
Array super_array table "super";
! Token that's impossible to type.
Array impossible_array table ".impossible";

! Those are used in place of a token's line to indicate special identifiers.
! (See comment in `InitCompiler`.)
! We should never get a source so big it's more than 2 billions lines (i.e. negative numbers in signed representation), so we're good.
Constant THIS_IDENTIFIER = -1;
Constant SUPER_IDENTIFIER = -2;
Constant IMPOSSIBLE_IDENTIFIER = -3;

! When we have to handle the special "this" and "super" cases.
[ TokenToAddr tok;
	switch (tok-->TOKEN_LINE) {
		! Adding WORDSIZE to skip entry 0 containing the size.
		THIS_IDENTIFIER: return this_array + WORDSIZE;
		SUPER_IDENTIFIER: return super_array + WORDSIZE;
		IMPOSSIBLE_IDENTIFIER: return impossible_array + WORDSIZE;
		default: return scanner.source + tok-->TOKEN_START * WORDSIZE;
	}
];

Object parser with
	current,
	previous,
	had_error,
	panic_mode,
;

! The index of the name of a local.
Constant LOCAL_NAME = 0;
! The index of the depth of a local.
Constant LOCAL_DEPTH = 1;
! Whether or not the local is captured by a closure.
Constant LOCAL_IS_CAPTURED = 2;
! The length of a local in words.
Constant LOCAL_SIZE = 3;

Constant UPVALUE_INDEX = 0; ! Number
Constant UPVALUE_IS_LOCAL = 1; ! Boolean
Constant UPVALUE_SIZE = 2;

! The types of function.
Constant TYPE_FUNCTION = 0;
Constant TYPE_INITIALIZER = TYPE_FUNCTION + 1;
Constant TYPE_METHOD = TYPE_INITIALIZER + 1;
Constant TYPE_SCRIPT = TYPE_METHOD + 1;

! The properties of a compiler.
Constant COMPILER_ENCLOSING = 0; ! Compiler
Constant COMPILER_FUNCTION = 1; ! ObjFunction
Constant COMPILER_TYPE = 2; ! Number
Constant COMPILER_LOCALS = 3; ! Array
Constant COMPILER_LOCAL_COUNT = 4; ! Number
Constant COMPILER_UPVALUES = 5; ! Upvalue[]
Constant COMPILER_SCOPE_DEPTH = 6; ! Number
Constant COMPILER_SIZE = 7;

Global current_compiler = 0;
Global current_class = 0;

[ NewCompiler  c x;
	c = COMPILER_SIZE * WORDSIZE;
	@malloc c c;
	if (c == 0) InfoloxFatalError("NewCompiler", "couldn't allocate memory");

	x = MAX_LOCALS * LOCAL_SIZE * WORDSIZE;
	@malloc x x;
	if (x == 0) InfoloxFatalError("NewCompiler", "couldn't allocate memory");
	c-->COMPILER_LOCALS = x;

	x = 256 * UPVALUE_SIZE * WORDSIZE;
	@malloc x x;
	if (x == 0) InfoloxFatalError("NewCompiler", "couldn't allocate memory");
	c-->COMPILER_UPVALUES = x;

	return c;
];

[ FreeCompiler c  x;
	x = c-->COMPILER_LOCALS;
	@mfree x;

	x = c-->COMPILER_UPVALUES;
	@mfree x;

	@mfree c;
];

! The properties of a class compiler.
Constant CLASSCOMPILER_ENCLOSING = 0; ! ClassCompiler
Constant CLASSCOMPILER_HAS_SUPERCLASS = CLASSCOMPILER_ENCLOSING + 1;
Constant CLASSCOMPILER_SIZE = CLASSCOMPILER_HAS_SUPERCLASS + 1;

[ NewClassCompiler  x;
	x = CLASSCOMPILER_SIZE * WORDSIZE;
	@malloc x x;
	if (x == 0) InfoloxFatalError("NewClassCompiler", "Couldn't allocate memory");
	x-->CLASSCOMPILER_ENCLOSING = 0;
	x-->CLASSCOMPILER_HAS_SUPERCLASS = false;
	return x;
];

[ EndClassCompiler  previous_class;
	previous_class = current_class;
	current_class = current_class-->CLASSCOMPILER_ENCLOSING;
	@mfree previous_class;
];

[ CurrentChunk;
	return current_compiler-->COMPILER_FUNCTION-->OBJFUNCTION_CHUNK;
];

[ ErrorAt tok msg  i tok_addr;
	if (parser.panic_mode) return;
	parser.panic_mode = true;
	print "[line ", tok-->TOKEN_LINE, "] Error";

	if (tok-->TOKEN_TYPE == TOKEN_EOF) {
		print " at end";
	} else if (tok-->TOKEN_TYPE == TOKEN_ERROR) {
		! Nothing.
	} else {
		print " at '";
		tok_addr = TokenToAddr(tok);
		for (i=0 : i<tok-->TOKEN_LENGTH : i++) {
			print (char) tok_addr-->i;
		}
		print "'";
	}

	print ": ", (string) msg, "^";
	parser.had_error = true;
];

[ Error msg;
	ErrorAt(parser.previous, msg);
];

[ ErrorAtCurrent msg;
	ErrorAt(parser.current, msg);
];

[ ParserAdvance;
	parser.previous = parser.current;

	while (true) {
		parser.current = ScanToken();
		if (parser.current-->TOKEN_TYPE ~= TOKEN_ERROR) {
			break;
		}

		ErrorAtCurrent(parser.current-->TOKEN_START);
	}
];

[ Consume t msg;
	if (parser.current-->TOKEN_TYPE == t) {
		ParserAdvance();
		return;
	}

	ErrorAtCurrent(msg);
];

[ Check t;
	return parser.current-->TOKEN_TYPE == t;
];

[ Match type;
	if ((~~Check(type))) rfalse;
	ParserAdvance();
	rtrue;
];

[ EmitByte byte;
	WriteChunk(CurrentChunk(), byte, parser.previous-->TOKEN_LINE);
];

[ EmitBytes byte1 byte2;
	EmitByte(byte1);
	EmitByte(byte2);
];

[ EmitLoop loop_start  offset x;
	EmitByte(OP_LOOP);

	offset = CurrentChunk()-->CHUNK_COUNT - loop_start + 2;
	if (offset > $FFFF) Error("Loop body too large.");

	@ushiftr offset 8 x;
	EmitByte(x & $FF);
	EmitByte(offset & $FF);
];

[ EmitJump instruction;
	EmitByte(instruction);
	EmitByte($FF);
	EmitByte($FF);
	return CurrentChunk()-->CHUNK_COUNT - 2;
];

[ EmitReturn;
	if (current_compiler-->COMPILER_TYPE == TYPE_INITIALIZER) {
		EmitBytes(OP_GET_LOCAL, 0);
	} else {
		EmitByte(OP_NIL);
	}
	EmitByte(OP_RETURN);
];

[ MakeConstant val_type value  const;
	const = AddConstant(CurrentChunk(), val_type, value);
	if (const > $FF) {
		Error("Too many constants in one chunk.");
		return 0;
	}

	return const;
];

[ EmitConstant val_type value;
	EmitBytes(OP_CONSTANT, MakeConstant(val_type, value));
];

[ PatchJump offset  jmp x;
	jmp = CurrentChunk()-->CHUNK_COUNT - offset - 2;

	if (jmp > $FFFF) {
		Error("Too much code to jump over.");
	}

	@ushiftr jmp 8 x;
	CurrentChunk()-->CHUNK_CODE->offset = x & $FF;
	CurrentChunk()-->CHUNK_CODE->(offset + 1) = jmp & $FF;
];

[ InitCompiler c t  tok locals;
	c-->COMPILER_ENCLOSING = current_compiler;
	c-->COMPILER_FUNCTION = 0;
	c-->COMPILER_TYPE = t;
	c-->COMPILER_LOCAL_COUNT = 0;
	c-->COMPILER_SCOPE_DEPTH = 0;
	c-->COMPILER_FUNCTION = NewFunction();
	current_compiler = c;
	if (t ~= TYPE_SCRIPT) {
		current_compiler-->COMPILER_FUNCTION-->OBJFUNCTION_NAME = CopyString(
			TokenToAddr(parser.previous),
			parser.previous-->TOKEN_LENGTH
		);
	}

	locals = current_compiler-->COMPILER_LOCALS;

	locals-->(current_compiler-->COMPILER_LOCAL_COUNT + LOCAL_DEPTH) = 0;
	locals-->(current_compiler-->COMPILER_LOCAL_COUNT + LOCAL_IS_CAPTURED) = false;
	tok = MakeToken(TOKEN_ERROR); ! TODO: Is that type OK? I guess it doesn't matter.
	if (t ~= TYPE_FUNCTION) {
		! TODO: Couldn't we use `SyntheticToken` here?
		tok-->TOKEN_START = "this";
		tok-->TOKEN_LENGTH = this_array-->0;
		! We use a special value in place of the line to indicate it's the `this` identifier.
		! This is because `TOKEN_START` is not a pointer but an index into the source.
		! And this special token does not point into the source.
		tok-->TOKEN_LINE = THIS_IDENTIFIER;
	} else {
		tok-->TOKEN_START = "";
		tok-->TOKEN_LENGTH = impossible_array-->0;
		tok-->TOKEN_LINE = IMPOSSIBLE_IDENTIFIER;
	}
	locals-->(current_compiler-->COMPILER_LOCAL_COUNT + LOCAL_NAME) = tok;
	current_compiler-->COMPILER_LOCAL_COUNT = current_compiler-->COMPILER_LOCAL_COUNT + 1;
];

! Ending the compiler does not destroy it! Because we may need to access its upvalues after ending it.
[ EndCompiler  f;
	EmitReturn();
	f = current_compiler-->COMPILER_FUNCTION;

	#Ifdef INFOLOX_PRINT_CODE;
	if ((~~parser.had_error)) {
		if (f-->OBJFUNCTION_NAME ~= 0) {
			DisassembleChunk(CurrentChunk(), f-->OBJFUNCTION_NAME-->OBJSTRING_CHARS);
		} else {
			DisassembleChunk(CurrentChunk(), "<script>");
		}
	}
	#Endif;

	current_compiler = current_compiler-->COMPILER_ENCLOSING;
	return f;
];

[ BeginScope;
	current_compiler-->COMPILER_SCOPE_DEPTH = current_compiler-->COMPILER_SCOPE_DEPTH + 1;
];

[ EndScope  x;
	current_compiler-->COMPILER_SCOPE_DEPTH = current_compiler-->COMPILER_SCOPE_DEPTH - 1;

	while (
		current_compiler-->COMPILER_LOCAL_COUNT > 0
		&& current_compiler-->COMPILER_LOCALS-->((current_compiler-->COMPILER_LOCAL_COUNT - 1) * LOCAL_SIZE + LOCAL_DEPTH) > current_compiler-->COMPILER_SCOPE_DEPTH
	) {
		if (current_compiler-->COMPILER_LOCALS-->((current_compiler-->COMPILER_LOCAL_COUNT - 1) * LOCAL_SIZE + LOCAL_IS_CAPTURED)) {
			EmitByte(OP_CLOSE_UPVALUE);
		} else {
			EmitByte(OP_POP);
			x = current_compiler-->COMPILER_LOCALS-->((current_compiler-->COMPILER_LOCAL_COUNT - 1) * LOCAL_SIZE + LOCAL_NAME);
		}
		current_compiler-->COMPILER_LOCAL_COUNT = current_compiler-->COMPILER_LOCAL_COUNT - 1;
	}
];

[ Binary  operator_type;
	operator_type = parser.previous-->TOKEN_TYPE;
	ParsePrecedence(GetPrecedence(operator_type) + 1);

	switch (operator_type) {
		TOKEN_PLUS: EmitByte(OP_ADD);
		TOKEN_MINUS: EmitByte(OP_SUBTRACT);
		TOKEN_STAR: EmitByte(OP_MULTIPLY);
		TOKEN_SLASH: EmitByte(OP_DIVIDE);
		TOKEN_BANG_EQUAL: EmitBytes(OP_EQUAL, OP_NOT);
		TOKEN_EQUAL_EQUAL: EmitByte(OP_EQUAL);
		TOKEN_GREATER: EmitByte(OP_GREATER);
		TOKEN_GREATER_EQUAL: EmitBytes(OP_LESS, OP_NOT);
		TOKEN_LESS: EmitByte(OP_LESS);
		TOKEN_LESS_EQUAL: EmitBytes(OP_GREATER, OP_NOT);
		default: InfoloxFatalError("Binary", "unreachable branch");
	}
];

! With an underscore because `call` is already a property (defined by the Inform 6 compiler).
[ Call_  arg_count;
	arg_count = ArgumentList();
	EmitBytes(OP_CALL, arg_count);
];

[ Dot can_assign  n arg_count;
	Consume(TOKEN_IDENTIFIER, "Expect property name after '.'.");
	n = IdentifierConstant(parser.previous);

	if (can_assign && Match(TOKEN_EQUAL)) {
		Expression();
		EmitBytes(OP_SET_PROPERTY, n);
	} else if (Match(TOKEN_LEFT_PAREN)) {
		arg_count = ArgumentList();
		EmitBytes(OP_INVOKE, n);
		EmitByte(arg_count);
	} else {
		EmitBytes(OP_GET_PROPERTY, n);
	}
];

[ Literal;
	switch (parser.previous-->TOKEN_TYPE) {
		TOKEN_FALSE: EmitByte(OP_FALSE);
		TOKEN_NIL: EmitByte(OP_NIL);
		TOKEN_TRUE: EmitByte(OP_TRUE);
		default: InfoloxFatalError("Literal", "unreachable branch");
	}
];

[ Grouping;
	Expression();
	Consume(TOKEN_RIGHT_PAREN, "Expect ')' after expression.");
];

[ Number  value;
	value = StrToFloat(TokenToAddr(parser.previous), parser.previous-->TOKEN_LENGTH);
	EmitConstant(VAL_NUMBER, value);
];

[ Or_  else_jump end_jump;
	else_jump = EmitJump(OP_JUMP_IF_FALSE);
	end_jump = EmitJump(OP_JUMP);

	PatchJump(else_jump);
	EmitByte(OP_POP);

	ParsePrecedence(PREC_OR);
	PatchJump(end_jump);
];

[ String_;
	EmitConstant(
		VAL_OBJ,
		CopyString(
			! The address of the start of the string.
			TokenToAddr(parser.previous) + WORDSIZE, ! Skip the opening quotation mark.
			! Its length in words.
			parser.previous-->TOKEN_LENGTH - 2
		)
	);
];

[ NamedVariable token can_assign  get_op set_op arg;
	arg = ResolveLocal(current_compiler, token);
	if (arg ~= -1) {
		get_op = OP_GET_LOCAL;
		set_op = OP_SET_LOCAL;
	} else if ((arg = ResolveUpvalue(current_compiler, token)) ~= -1) {
		get_op = OP_GET_UPVALUE;
		set_op = OP_SET_UPVALUE;
	} else {
		arg = IdentifierConstant(token);
		get_op = OP_GET_GLOBAL;
		set_op = OP_SET_GLOBAL;
	}

	if (can_assign && Match(TOKEN_EQUAL)) {
		Expression();
		EmitBytes(set_op, arg);
	} else {
		EmitBytes(get_op, arg);
	}
];

[ Variable can_assign;
	NamedVariable(parser.previous, can_assign);
];

[ SyntheticToken n l special_value  tok;
	tok = MakeToken(TOKEN_ERROR); ! TODO: Is that type OK? I guess it doesn't matter.
	tok-->TOKEN_START = n;
	tok-->TOKEN_LENGTH = l;
	tok-->TOKEN_LINE = special_value;
	return tok;
];

[ Super  n  arg_count;
	if (current_class == 0) {
		Error("Can't use 'super' outside of a class.");
	} else if (current_class-->CLASSCOMPILER_HAS_SUPERCLASS == false) {
		Error("Can't use 'super' in a class with no superclass.");
	}
	Consume(TOKEN_DOT, "Expect '.' after 'super'.");
	Consume(TOKEN_IDENTIFIER, "Expect superclass method name.");
	n = IdentifierConstant(parser.previous);

	NamedVariable(SyntheticToken("this", 4, THIS_IDENTIFIER));
	if (Match(TOKEN_LEFT_PAREN)) {
		arg_count = ArgumentList();
		NamedVariable(SyntheticToken("super", 5, SUPER_IDENTIFIER));
		EmitBytes(OP_SUPER_INVOKE, n);
		EmitByte(arg_count);
	} else {
		NamedVariable(SyntheticToken("super", 5, SUPER_IDENTIFIER));
		EmitBytes(OP_GET_SUPER, n);
	}
];

[ This;
	if (current_class == 0) {
		Error("Can't use 'this' outside of a class.");
		return;
	}

	Variable(false);
];

[ Unary  operator_type;
	operator_type = parser.previous-->TOKEN_TYPE;

	! Compile the operand.
	ParsePrecedence(PREC_UNARY);

	! Emit the operator instruction.
	switch (operator_type) {
		TOKEN_BANG: EmitByte(OP_NOT);
		TOKEN_MINUS: EmitByte(OP_NEGATE);
		default: InfoloxFatalError("Unary", "unreachable branch");
	}
];

[ GetPrecedence operator;
	switch (operator) {
		TOKEN_DOT, TOKEN_LEFT_PAREN: return PREC_CALL;
		TOKEN_MINUS, TOKEN_PLUS: return PREC_TERM;
		TOKEN_SLASH, TOKEN_STAR: return PREC_FACTOR;
		TOKEN_BANG_EQUAL, TOKEN_EQUAL_EQUAL: return PREC_EQUALITY;
		TOKEN_GREATER, TOKEN_GREATER_EQUAL, TOKEN_LESS, TOKEN_LESS_EQUAL: return PREC_COMPARISON;
		TOKEN_AND: return PREC_AND;
		TOKEN_OR: return PREC_OR;
		default: return PREC_NONE;
	}
];

[ GetPrefixRule operator_type;
	switch (operator_type) {
		TOKEN_LEFT_PAREN: return Grouping;
		TOKEN_MINUS, TOKEN_BANG: return Unary;
		TOKEN_NUMBER: return Number;
		TOKEN_FALSE, TOKEN_TRUE, TOKEN_NIL: return Literal;
		TOKEN_IDENTIFIER: return Variable;
		TOKEN_STRING: return String_;
		TOKEN_SUPER: return Super;
		TOKEN_THIS: return This;
		default: return 0;
	}
];

[ GetInfixRule operator_type;
	switch (operator_type) {
		TOKEN_LEFT_PAREN: return Call_;
		TOKEN_DOT: return Dot;
		TOKEN_MINUS, TOKEN_PLUS, TOKEN_SLASH, TOKEN_STAR, TOKEN_BANG_EQUAL, TOKEN_EQUAL_EQUAL, TOKEN_GREATER, TOKEN_GREATER_EQUAL, TOKEN_LESS, TOKEN_LESS_EQUAL: return Binary;
		TOKEN_AND: return And;
		TOKEN_OR: return Or_;
		default: return 0;
	}
];

[ ParsePrecedence precedence  rule can_assign;
	ParserAdvance();
	rule = GetPrefixRule(parser.previous-->TOKEN_TYPE);
	if (~~rule) {
		Error("Expect expression.");
		return;
	}
	can_assign = precedence <= PREC_ASSIGNMENT;
	rule(can_assign);

	while (precedence <= GetPrecedence(parser.current-->TOKEN_TYPE)) {
		ParserAdvance();
		rule = GetInfixRule(parser.previous-->TOKEN_TYPE);
		rule(can_assign);
	}

	if (can_assign && Match(TOKEN_EQUAL)) {
		Error("Invalid assignment target.");
	}
];

[ IdentifierConstant token;
	return MakeConstant(
		VAL_OBJ,
		CopyString(TokenToAddr(token), token-->TOKEN_LENGTH)
	);
];

[ IdentifiersEqual a b;
	if (a-->TOKEN_LENGTH ~= b-->TOKEN_LENGTH) rfalse;
	return memcmp(TokenToAddr(a), TokenToAddr(b), a-->TOKEN_LENGTH);
];

[ ResolveLocal c tok  i local;
	for (i=c-->COMPILER_LOCAL_COUNT-1 : i>=0 : i--) {
		! The address of the local.
		local = c-->COMPILER_LOCALS + i * LOCAL_SIZE * WORDSIZE;
		if (IdentifiersEqual(tok, local-->LOCAL_NAME)) {
			if (local-->LOCAL_DEPTH == -1) {
				Error("Can't read local variable in its own initializer.");
			}
			return i;
		}
	}

	return -1;
];

[ AddUpvalue c idx is_loc  count i uv;
	count = c-->COMPILER_FUNCTION-->OBJFUNCTION_UPVALUE_COUNT;

	for (i=0 : i<count : i++) {
		uv = c-->COMPILER_UPVALUES + i * UPVALUE_SIZE * WORDSIZE;
		if (uv-->UPVALUE_INDEX == idx && uv-->UPVALUE_IS_LOCAL == is_loc) {
			return i;
		}
	}

	if (count == 256) {
		Error("Too many closure variables in function.");
		return 0;
	}

	uv = c-->COMPILER_UPVALUES + count * UPVALUE_SIZE * WORDSIZE;
	uv-->UPVALUE_IS_LOCAL = is_loc;
	uv-->UPVALUE_INDEX = idx;

	i = c-->COMPILER_FUNCTION-->OBJFUNCTION_UPVALUE_COUNT;
	c-->COMPILER_FUNCTION-->OBJFUNCTION_UPVALUE_COUNT = i + 1;
	return i;
];

[ ResolveUpvalue c tok  local uv;
	if (c-->COMPILER_ENCLOSING == 0) return -1;

	local = ResolveLocal(c-->COMPILER_ENCLOSING, tok);
	if (local ~= -1) {
		c-->COMPILER_ENCLOSING-->COMPILER_LOCALS-->(local * LOCAL_SIZE + LOCAL_IS_CAPTURED) = true;
		return AddUpvalue(c, local, true);
	}

	uv = ResolveUpvalue(c-->COMPILER_ENCLOSING, tok);
	if (uv ~= -1) {
		return AddUpvalue(c, uv, false);
	}

	return -1;
];

[ AddLocal tok  local;
	if (current_compiler-->COMPILER_LOCAL_COUNT == MAX_LOCALS) {
		Error("Too many local variables in function.");
		return;
	}

	! The address of the local, not the index in the locals array.
	local = current_compiler-->COMPILER_LOCALS + current_compiler-->COMPILER_LOCAL_COUNT * LOCAL_SIZE * WORDSIZE;
	current_compiler-->COMPILER_LOCAL_COUNT = current_compiler-->COMPILER_LOCAL_COUNT + 1;
	local-->LOCAL_NAME = tok;
	local-->LOCAL_DEPTH = -1;
	local-->LOCAL_IS_CAPTURED = false;
];

[ DeclareVariable  tok local i;
	if (current_compiler-->COMPILER_SCOPE_DEPTH == 0) return;

	tok = parser.previous;
	for (i=current_compiler-->COMPILER_LOCAL_COUNT-1 : i>=0 : i--) {
		! The address of the local, not the index in the locals array.
		local = current_compiler-->COMPILER_LOCALS + i * LOCAL_SIZE * WORDSIZE;
		if (local-->LOCAL_DEPTH ~= -1 && local-->LOCAL_DEPTH < current_compiler-->COMPILER_SCOPE_DEPTH) {
			break;
		}

		if (IdentifiersEqual(tok, local-->LOCAL_NAME)) {
			Error("Already a variable with this name in this scope.");
		}
	}

	AddLocal(tok);
];

[ ParseVariable error_message;
	Consume(TOKEN_IDENTIFIER, error_message);

	DeclareVariable();
	if (current_compiler-->COMPILER_SCOPE_DEPTH > 0) return 0;

	return IdentifierConstant(parser.previous);
];

[ MarkInitialized;
	if (current_compiler-->COMPILER_SCOPE_DEPTH == 0) return;
	current_compiler-->COMPILER_LOCALS-->((current_compiler-->COMPILER_LOCAL_COUNT - 1) * LOCAL_SIZE + LOCAL_DEPTH) = current_compiler-->COMPILER_SCOPE_DEPTH;
];

[ DefineVariable glob;
	if (current_compiler-->COMPILER_SCOPE_DEPTH > 0) {
		MarkInitialized();
		return;
	}

	EmitBytes(OP_DEFINE_GLOBAL, glob);
];

[ ArgumentList  arg_count;
	if ((~~Check(TOKEN_RIGHT_PAREN))) {
		do {
			Expression();
			if (arg_count == 255) {
				Error("Can't have more than 255 arguments.");
			}
			arg_count++;
		} until ((~~Match(TOKEN_COMMA)));
	}
	Consume(TOKEN_RIGHT_PAREN, "Expect ')' after arguments.");
	return arg_count;
];

[ And  end_jump;
	end_jump = EmitJump(OP_JUMP_IF_FALSE);

	EmitByte(OP_POP);
	ParsePrecedence(PREC_AND);

	PatchJump(end_jump);
];

[ Expression;
	ParsePrecedence(PREC_ASSIGNMENT);
];

[ Block;
	while ((~~Check(TOKEN_RIGHT_BRACE)) && (~~Check(TOKEN_EOF))) {
		Declaration();
	}

	Consume(TOKEN_RIGHT_BRACE, "Expect '}' after block.");
];

! It's not named `Function` because that was already the name of a property, when a lot of things were still preallocated Inform 6 objects instead of memory allocated on the heap.
! TODO: I could change that now, but everything works so I'll avoid touching things for the moment.
[ CompileFunction t  c f const i uv;
	c = NewCompiler();
	InitCompiler(c, t);
	BeginScope();

	Consume(TOKEN_LEFT_PAREN, "Expect '(' after function name.");
	if ((~~Check(TOKEN_RIGHT_PAREN))) {
		do {
			current_compiler-->COMPILER_FUNCTION-->OBJFUNCTION_ARITY = current_compiler-->COMPILER_FUNCTION-->OBJFUNCTION_ARITY + 1;
			if (current_compiler-->COMPILER_FUNCTION-->OBJFUNCTION_ARITY > 255) {
				ErrorAtCurrent("Can't have more than 255 parameters.");
			}
			const = ParseVariable("Expect parameter name.");
			DefineVariable(const);
		} until ((~~Match(TOKEN_COMMA)));
	}
	Consume(TOKEN_RIGHT_PAREN, "Expect ')' after parameters.");
	Consume(TOKEN_LEFT_BRACE, "Expect '{' before function body.");
	Block();

	f = EndCompiler();
	EmitBytes(OP_CLOSURE, MakeConstant(VAL_OBJ, f));

	for (i=0 : i<f-->OBJFUNCTION_UPVALUE_COUNT : i++) {
		uv = c-->COMPILER_UPVALUES + i * UPVALUE_SIZE * WORDSIZE;
		if (uv-->UPVALUE_IS_LOCAL) {
			EmitByte(1);
		} else {
			EmitByte(0);
		}
		EmitByte(uv-->UPVALUE_INDEX);
	}
	! And then free the previous compiler.
	! I believe freeing the compiler at this point is OK.
	FreeCompiler(c);
];

[ Method  const f_type;
	Consume(TOKEN_IDENTIFIER, "Expect method name.");
	const = IdentifierConstant(parser.previous);

	f_type = TYPE_METHOD;
	if (
		parser.previous-->TOKEN_LENGTH == 4
		&& memcmp(TokenToAddr(parser.previous), init_array + WORDSIZE, 4)
	) {
		f_type = TYPE_INITIALIZER;
	}
	CompileFunction(f_type);
	EmitBytes(OP_METHOD, const);
];

[ ClassDeclaration  class_name name_constant class_compiler;
	Consume(TOKEN_IDENTIFIER, "Expect class name.");
	class_name = parser.previous;
	name_constant = IdentifierConstant(parser.previous);
	DeclareVariable();

	EmitBytes(OP_CLASS, name_constant);
	DefineVariable(name_constant);

	class_compiler = NewClassCompiler();
	class_compiler-->CLASSCOMPILER_ENCLOSING = current_class;
	current_class = class_compiler;

	if (Match(TOKEN_LESS)) {
		Consume(TOKEN_IDENTIFIER, "Expect superclass name.");
		Variable(false);

		if (IdentifiersEqual(class_name, parser.previous)) {
			Error("A class can't inherit from itself.");
		}

		BeginScope();
		AddLocal(SyntheticToken("super", 5, SUPER_IDENTIFIER));
		DefineVariable(0);

		NamedVariable(class_name);
		EmitByte(OP_INHERIT);
		class_compiler-->CLASSCOMPILER_HAS_SUPERCLASS = true;
	}

	NamedVariable(class_name);
	Consume(TOKEN_LEFT_BRACE, "Expect '{' before class body.");
	while (Check(TOKEN_RIGHT_BRACE) == 0 && Check(TOKEN_EOF) == 0) {
		Method();
	}
	Consume(TOKEN_RIGHT_BRACE, "Expect '}' after class body.");
	EmitByte(OP_POP);

	if (class_compiler-->CLASSCOMPILER_HAS_SUPERCLASS) {
		EndScope();
	}

	EndClassCompiler();
];

[ FunDeclaration  glob;
	glob = ParseVariable("Expect function name.");
	MarkInitialized();
	CompileFunction(TYPE_FUNCTION);
	DefineVariable(glob);
];

[ VarDeclaration  glob;
	glob = ParseVariable("Expect variable name.");

	if (Match(TOKEN_EQUAL)) {
		Expression();
	} else {
		EmitByte(OP_NIL);
	}
	Consume(TOKEN_SEMICOLON, "Expect ';' after variable declaration.");

	DefineVariable(glob);
];

[ ExpressionStatement;
	Expression();
	Consume(TOKEN_SEMICOLON, "Expect ';' after expression.");
	EmitByte(OP_POP);
];

[ ForStatement  loop_start exit_jump body_jump increment_start;
	BeginScope();
	Consume(TOKEN_LEFT_PAREN, "Expect '(' after 'for'.");
	if (Match(TOKEN_SEMICOLON)) {
		! No initializer.
	} else if (Match(TOKEN_VAR)) {
		VarDeclaration();
	} else {
		ExpressionStatement();
	}

	loop_start = CurrentChunk()-->CHUNK_COUNT;
	exit_jump = -1;
	if ((~~Match(TOKEN_SEMICOLON))) {
		Expression();
		Consume(TOKEN_SEMICOLON, "Expect ';' after loop condition.");

		! Jump out of the loop if the condition is false.
		exit_jump = EmitJump(OP_JUMP_IF_FALSE);
		EmitByte(OP_POP); ! Condition.
	}

	if ((~~Match(TOKEN_RIGHT_PAREN))) {
		body_jump = EmitJump(OP_JUMP);
		increment_start = CurrentChunk()-->CHUNK_COUNT;
		Expression();
		EmitByte(OP_POP);
		Consume(TOKEN_RIGHT_PAREN, "Expect ')' after for clauses.");

		EmitLoop(loop_start);
		loop_start = increment_start;
		PatchJump(body_jump);
	}

	Statement();
	EmitLoop(loop_start);

	if (exit_jump ~= -1) {
		PatchJump(exit_jump);
		EmitByte(OP_POP); ! Condition.
	}

	EndScope();
];

[ IfStatement  then_jump else_jump;
	Consume(TOKEN_LEFT_PAREN, "Expect '(' after 'if'.");
	Expression();
	Consume(TOKEN_RIGHT_PAREN, "Expect ')' after condition.");

	then_jump = EmitJump(OP_JUMP_IF_FALSE);
	EmitByte(OP_POP);
	Statement();

	else_jump = EmitJump(OP_JUMP);

	PatchJump(then_jump);
	EmitByte(OP_POP);

	if (Match(TOKEN_ELSE)) Statement();
	PatchJump(else_jump);
];

[ PrintStatement;
	Expression();
	Consume(TOKEN_SEMICOLON, "Expect ';' after value.");
	EmitByte(OP_PRINT);
];

[ ReturnStatement;
	if (current_compiler-->COMPILER_TYPE == TYPE_SCRIPT) {
		Error("Can't return from top-level code.");
	}

	if (Match(TOKEN_SEMICOLON)) {
		EmitReturn();
	} else {
		if (current_compiler-->COMPILER_TYPE == TYPE_INITIALIZER) {
			Error("Can't return a value from an initializer.");
		}

		Expression();
		Consume(TOKEN_SEMICOLON, "Expect ';' after return value.");
		EmitByte(OP_RETURN);
	}
];

[ WhileStatement  exit_jump loop_start;
	loop_start = CurrentChunk()-->CHUNK_COUNT;
	Consume(TOKEN_LEFT_PAREN, "Expect '(' after 'while'.");
	Expression();
	Consume(TOKEN_RIGHT_PAREN, "Expect ')' after condition.");

	exit_jump = EmitJump(OP_JUMP_IF_FALSE);
	EmitByte(OP_POP);
	Statement();
	EmitLoop(loop_start);

	PatchJump(exit_jump);
	EmitByte(OP_POP);
];

[ Synchronize;
	parser.panic_mode = false;

	while (parser.current-->TOKEN_TYPE ~= TOKEN_EOF) {
		if (parser.previous-->TOKEN_TYPE == TOKEN_SEMICOLON) return;
		switch (parser.current-->TOKEN_TYPE) {
			TOKEN_CLASS, TOKEN_FUN, TOKEN_VAR, TOKEN_FOR, TOKEN_IF, TOKEN_WHILE, TOKEN_PRINT, TOKEN_RETURN: return;
			default: ; ! Do nothing.
		}

		ParserAdvance();
	}
];

[ Declaration;
	if (Match(TOKEN_CLASS)) {
		ClassDeclaration();
	} else if (Match(TOKEN_FUN)) {
		FunDeclaration();
	} else if (Match(TOKEN_VAR)) {
		VarDeclaration();
	} else {
		Statement();
	}

	if (parser.panic_mode) Synchronize();
];

[ Statement;
	if (Match(TOKEN_PRINT)) {
		PrintStatement();
	} else if (Match(TOKEN_IF)) {
		IfStatement();
	} else if (Match(TOKEN_return)) {
		ReturnStatement();
	} else if (Match(TOKEN_WHILE)) {
		WhileStatement();
	} else if (Match(TOKEN_FOR)) {
		ForStatement();
	} else if (Match(TOKEN_LEFT_BRACE)) {
		BeginScope();
		Block();
		EndScope();
	} else {
		ExpressionStatement();
	}
];

[ Compile source  c f;
	InitScanner(source);
	c = NewCompiler();
	InitCompiler(c, TYPE_SCRIPT);

	parser.had_error = false;
	parser.panic_mode = false;

	ParserAdvance();

	while ((~~Match(TOKEN_EOF))) {
		Declaration();
	}

	f = EndCompiler();
	! And then free the compiler.
	! I believe freeing the compiler at this point is OK.
	FreeCompiler(c);
	if (parser.had_error) return 0;
	else return f;
];

[ MarkCompilerRoots  c;
	c = current_compiler;
	while (c ~= 0) {
		MarkObject(c-->COMPILER_FUNCTION);
		c = c-->COMPILER_ENCLOSING;
	}
];

! A crude way to convert an array containing characters in the form "\d+(\.\d+)?" to a float.
! In particular, it won't be accurate if the integer part or the decimal part exceed 9 digits.
! I may or may not improve this routine one day; for the moment, it does the job.
! TODO: Make it a Lox error to have a number literal with too many digits? It's less flexible, but users won't be caught off-guard.
Constant PARSE_MAX_DIGITS = 9;
[ StrToFloat arr length  i c int_part dec_part int_place dec_place;
	! Skip leading zeros.
	while (arr-->i == '0' && i < length) {
		i++;
	}

	! Scan the remaining characters.
	dec_part = -1;
	int_place = -PARSE_MAX_DIGITS;
	for (: i<length : i++) {
		c = arr-->i;
		switch (c) {
			! A digit. Accumulate them in a normal 32-bit integer.
			! If it doesn't fit in, track the number of digits that don't fit.
			'0' to '9': if (dec_part == -1) { ! We're in the integer part.
				if (int_place < 0) int_part = int_part * 10 + (c - '0');
				int_place++;
			} else if (dec_place < PARSE_MAX_DIGITS) { ! We're in the decimal part.
					dec_part = dec_part * 10 + (c - '0');
					dec_place++;
			}
			! A dot. Start parsing the decimal part if we're not already.
			'.': if (dec_part == -1) {
				dec_part = 0;
			} else {
				InfoloxFatalError("StrToFloat", "unexpected character '.'");
			}
			! Any other characters. Should never happen.
			default: InfoloxFatalError("StrToFloat", "unexpected character");
		}
	}

	! Convert the integer part to a float.
	@numtof int_part int_part;
	! If the number was too big to fit in a 32-bit integer, multiply by a power of 10 to add missing digits.
	! That's where we lose information since the extra digits the user typed are replaced by zeros.
	if (int_place > 0) {
		@numtof int_place int_place;
		@pow $+10 int_place int_place;
		@fmul int_part int_place int_part;
	}

	! If there was a decimal part, do the same thing, except that we divide to make it smaller than 1.
	if (dec_part ~= -1) {
		@numtof dec_part dec_part;
		@numtof dec_place dec_place;
		@pow $+10 dec_place dec_place;
		! We'll never divide by 0 because the parser guarantees that there is at least a digit after the dot.
		@fdiv dec_part dec_place dec_part;

		! And we add the integer and decimal parts together.
		@fadd int_part dec_part int_part;
	}

	return int_part;
];
