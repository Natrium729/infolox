Constant OP_CONSTANT = 0;
Constant OP_NIL = OP_CONSTANT + 1;
Constant OP_TRUE = OP_NIL + 1;
Constant OP_FALSE = OP_TRUE + 1;
Constant OP_POP = OP_FALSE + 1;
Constant OP_GET_LOCAL = OP_POP + 1;
Constant OP_SET_LOCAL = OP_GET_LOCAL + 1;
Constant OP_GET_GLOBAL = OP_SET_LOCAL + 1;
Constant OP_DEFINE_GLOBAL = OP_GET_GLOBAL + 1;
Constant OP_SET_GLOBAL = OP_DEFINE_GLOBAL + 1;
Constant OP_GET_UPVALUE = OP_SET_GLOBAL + 1;
Constant OP_SET_UPVALUE = OP_GET_UPVALUE + 1;
Constant OP_GET_PROPERTY = OP_SET_UPVALUE + 1;
Constant OP_SET_PROPERTY = OP_GET_PROPERTY + 1;
Constant OP_GET_SUPER = OP_SET_PROPERTY + 1;
Constant OP_EQUAL = OP_GET_SUPER + 1;
Constant OP_GREATER = OP_EQUAL + 1;
Constant OP_LESS = OP_GREATER + 1;
Constant OP_ADD = OP_LESS + 1;
Constant OP_SUBTRACT = OP_ADD + 1;
Constant OP_MULTIPLY = OP_SUBTRACT + 1;
Constant OP_DIVIDE = OP_MULTIPLY + 1;
Constant OP_NOT = OP_DIVIDE + 1;
Constant OP_NEGATE = OP_NOT + 1;
Constant OP_PRINT = OP_NEGATE + 1;
Constant OP_JUMP = OP_PRINT + 1;
Constant OP_JUMP_IF_FALSE = OP_JUMP + 1;
Constant OP_LOOP = OP_JUMP_IF_FALSE + 1;
Constant OP_CALL = OP_LOOP + 1;
Constant OP_INVOKE = OP_CALL + 1;
Constant OP_SUPER_INVOKE = OP_INVOKE + 1;
Constant OP_CLOSURE = OP_SUPER_INVOKE + 1;
Constant OP_CLOSE_UPVALUE = OP_CLOSURE + 1;
Constant OP_RETURN = OP_CLOSE_UPVALUE + 1;
Constant OP_CLASS = OP_RETURN + 1;
Constant OP_INHERIT = OP_CLASS + 1;
Constant OP_METHOD = OP_INHERIT + 1;


Constant CHUNK_COUNT = 0; ! Number
Constant CHUNK_CAPACITY = 1; ! Number
Constant CHUNK_CODE = 2; ! Byte[]
Constant CHUNK_LINES = 3; ! Number[]
Constant CHUNK_CONSTANTS= 4; ! ValueArray
Constant CHUNK_SIZE = 5;

[ NewChunk  x;
	x = CHUNK_SIZE * WORDSIZE;
	@malloc x x;
	if (x == 0) InfoloxFatalError("NewChunk", "couldn't allocate memory");
	InitChunk(x);
	return x;
];

[ InitChunk chunk;
	chunk-->CHUNK_COUNT = 0;
	chunk-->CHUNK_CAPACITY = 0;
	chunk-->CHUNK_CODE = 0;
	chunk-->CHUNK_LINES = 0;
	chunk-->CHUNK_CONSTANTS = NewValueArray();
];

[ FreeChunk chunk;
	FreeArray(1, chunk-->CHUNK_CODE, chunk-->CHUNK_CAPACITY);
	FreeArray(WORDSIZE, chunk-->CHUNK_LINES, chunk-->CHUNK_CAPACITY);
	FreeValueArray(chunk-->CHUNK_CONSTANTS);
	@mfree chunk;
];

[ WriteChunk chunk byte line  old_capacity;
	if (chunk-->CHUNK_CAPACITY < chunk-->CHUNK_COUNT + 1) {
		old_capacity = chunk-->CHUNK_CAPACITY;
		chunk-->CHUNK_CAPACITY = GrowCapacity(old_capacity);
		chunk-->CHUNK_CODE = GrowArray(1, chunk-->CHUNK_CODE, old_capacity, chunk-->CHUNK_CAPACITY);
		chunk-->CHUNK_LINES = GrowArray(WORDSIZE, chunk-->CHUNK_LINES, old_capacity, chunk-->CHUNK_CAPACITY);
	}

	chunk-->CHUNK_CODE->(chunk-->CHUNK_COUNT) = byte;
	chunk-->CHUNK_LINES-->(chunk-->CHUNK_COUNT) = line;
	chunk-->CHUNK_COUNT = chunk-->CHUNK_COUNT + 1;
];

[ AddConstant chunk val_type value;
	Push(val_type, value);
	WriteValueArray(chunk-->CHUNK_CONSTANTS, val_type, value);
	Pop();
	return chunk-->CHUNK_CONSTANTS-->VALUE_ARRAY_COUNT - 1;
];
