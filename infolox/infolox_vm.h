Default FRAMES_MAX = 64;
Constant STACK_MAX = FRAMES_MAX * $FF;

Constant INTERPRET_OK = 0;
Constant INTERPRET_COMPILE_ERROR = INTERPRET_OK + 1;
Constant INTERPRET_RUNTIME_ERROR = INTERPRET_COMPILE_ERROR + 1;

Class CallFrame(FRAMES_MAX) with
	closure,
	ip,
	! The address of the first value of the frame on the stack.
	slots,
;

Object vm with
	frames,
	frame_count,
	stack,

	! That's the value-index of the top of the stack. To get the offset in words (when using the `-->` operator), we should not forget to multiply by `VALUE_SIZE`!
	stack_top,

	globals,
	strings,
	init_string,
	open_upvalues,

	bytes_allocated,
	next_gc,
	objects,
	gray_count,
	gray_capacity,
	gray_stack,
;

! Will contain the timestamp at which a script started.
Global clock_zero_s = 0; ! Stored as an int.
Global clock_zero_micros = 0; ! Stored as a float.

Array clock_glk_result --> 3;
Array clock_result --> VAL_NUMBER 0; ! The clock native function always returns a number.

Array clock_native_name table "clock";
[ ClockNative  delta micros;
	glk($160, clock_glk_result); ! glk_current_time

	delta = (clock_glk_result-->1 & $7FFFFFFF) - clock_zero_s;
	@numtof delta delta;

	micros = clock_glk_result-->2;
	@numtof micros micros;
	@fdiv micros $+1000000 micros;
	@fsub micros clock_zero_micros micros;

	@fadd delta micros delta;
	clock_result-->1 = delta;
	return clock_result;
];

[ ResetStack;
	vm.stack_top = 0;
	vm.frame_count = 0;
	vm.open_upvalues = 0;
];

! Sometimes, when we need a more complex message that a simple string, we print it before calling this function with an empty string.
! It's not elegant, but it works.
[ RuntimeError msg  i frame f;
	print (string) msg, "^";

	for (i=vm.frame_count-1 : i>=0 : i--) {
		frame = vm.frames-->i;
		f = frame.closure-->OBJCLOSURE_FUNCTION;
		print "[line ", f-->OBJFUNCTION_CHUNK-->CHUNK_LINES-->frame.ip, "] in ";
		if (f-->OBJFUNCTION_NAME == 0) {
			print "script^";
		} else {
			print (PrintObject) f-->OBJFUNCTION_NAME, "()^";
		}
	}

	ResetStack();
];

! `n` is an adress of an array containing the characters of the function's name, with the zeroth entry being the length.
[ DefineNative n native_f;
	Push(VAL_OBJ, CopyString(n + WORDSIZE, n-->0));
	Push(VAL_OBJ, NewNative(native_f));
	TableSet(
		vm.globals,
		vm.stack-->1, ! Get the string object for the key.
		vm.stack-->VALUE_SIZE, ! Get the type of the function.
		vm.stack-->(VALUE_SIZE + 1) ! Get the value of the function.
	);
	Pop();
	Pop();
];

[ InitVM  x;
	! Allocate the call frames.
	x = FRAMES_MAX * WORDSIZE;
	@malloc x x;
	if (x == 0) InfoloxFatalError("InitVM", "couldn't allocate memory");
	vm.frames = x;
	for (x=0 : x<FRAMES_MAX : x++) {
		vm.frames-->x = CallFrame.create();
	}

	! Allocate the stack.
	x = VALUE_SIZE * STACK_MAX * WORDSIZE;
	@malloc x x;
	if (x == 0) InfoloxFatalError("InitVM", "couldn't allocate memory");
	vm.stack = x;

	ResetStack();
	vm.objects = 0;
	vm.bytes_allocated = 0;
	vm.next_gc =  1024 * 1024;

	vm.gray_count = 0;
	vm.gray_capacity = 0;
	vm.gray_stack = 0;

	vm.globals = NewTable();
	vm.strings = NewTable();

	vm.init_string = 0;
	vm.init_string = CopyString(init_array + WORDSIZE, init_array-->0);

	DefineNative(clock_native_name, ClockNative);
	! Set the current time as the starting time, that `ClockNative` will use.
	! We have to do that because the number of seconds since 1970 is to big to fit with enough precision into a 32-bit float.
	glk($160, clock_glk_result); ! glk_current_time
	! We only use the low part of the seconds for the sake of simplicity.
	! It will be OK most of the time because `ClockNative` computes the elapsed time since the start of a script.
	! It will only give an invalid result when if the high part changed since that moment, which won't happen often, to say the least.
	! And since `@numtof` considers the integer signed, but the number of seconds we get is unsigned, we turn off the sign bit.
	! Again, it will cause problems if the overflow happens since the start of the script.
	clock_zero_s = clock_glk_result-->1 & $7FFFFFFF;
	x = clock_glk_result-->2;
	@numtof x x;
	@fdiv x $+1000000 x;
	clock_zero_micros = x;
];

[ FreeVM  x;
	for (x=0 : x<FRAMES_MAX : x++) {
		CallFrame.destroy(vm.frames-->x);
	}
	x = vm.frames;
	@mfree x;
	x = vm.stack;
	@mfree x;
	vm.stack = 0;
	FreeTable(vm.globals);
	FreeTable(vm.strings);
	vm.init_string = 0;
	FreeObjects();
];

[ Push val_type value;
	vm.stack-->(VALUE_SIZE * vm.stack_top) = val_type;
	vm.stack-->(VALUE_SIZE * vm.stack_top + 1) = value;
	vm.stack_top++;
];

[ Pop;
	vm.stack_top--;
	return vm.stack-->(VALUE_SIZE * vm.stack_top + 1);
];

[ PeekType distance;
	return vm.stack-->(VALUE_SIZE * (vm.stack_top - 1 - distance));
];

[ PeekValue distance;
	return vm.stack-->(VALUE_SIZE * (vm.stack_top - 1 - distance) + 1);
];

! This routine should have bee called `Call_`, but `infolox_compiler.h` already defines a function with that name.
[ CallClosure c arg_count  frame;
	if (arg_count ~= c-->OBJCLOSURE_FUNCTION-->OBJFUNCTION_ARITY) {
		print "Expected ", c-->OBJCLOSURE_FUNCTION-->OBJFUNCTION_ARITY, " arguments but got ", arg_count, ".";
		RuntimeError("");
		rfalse;
	}

	if (vm.frame_count == FRAMES_MAX) {
		RuntimeError("Stack overflow.");
		rfalse;
	}

	frame = vm.frames-->(vm.frame_count++);
	frame.closure = c;
	frame.ip = 0;
	frame.slots = vm.stack + (vm.stack_top - arg_count - 1) * VALUE_SIZE * WORDSIZE;
	rtrue;
];

[ CallValue callee_type callee arg_count  native result;
	if (callee_type == VAL_OBJ) {
		switch (callee-->OBJ_TYPE) {
			OBJ_BOUND_METHOD:
				vm.stack-->((vm.stack_top - arg_count - 1) * VALUE_SIZE) = VAL_OBJ;
				vm.stack-->((vm.stack_top - arg_count - 1) * VALUE_SIZE + 1) = callee-->OBJBOUNDMETHOD_RECEIVER;
				return CallClosure(callee-->OBJBOUNDMETHOD_METHOD, arg_count);
			OBJ_CLASS:
				vm.stack-->((vm.stack_top - arg_count - 1) * VALUE_SIZE) = VAL_OBJ;
				vm.stack-->((vm.stack_top - arg_count - 1) * VALUE_SIZE + 1) = NewInstance(callee);
				if (TableGet(callee-->OBJCLASS_METHODS, vm.init_string)) {
					return CallClosure(TableGet_value, arg_count);
				} else if (arg_count ~= 0) {
					print "Expected 0 arguments but got ", arg_count, ".";
					RuntimeError("");
					rfalse;
				}
				rtrue;
			OBJ_CLOSURE:
				return CallClosure(callee, arg_count);
			OBJ_NATIVE:
				native = callee-->OBJNATIVE_FUNCTION;
				result = native(
					arg_count,
					! The address of the first argument on the stack.
					vm.stack + (vm.stack_top - arg_count) * VALUE_SIZE * WORDSIZE
				);
				vm.stack_top = vm.stack_top - arg_count - 1;
				Push(result-->0, result-->1);
				rtrue;
			default: ;
		}
	}
	RuntimeError("Can only call functions and classes.");
	rfalse;
];

[ InvokeFromClass klass n arg_count;
	if (TableGet(klass-->OBJCLASS_METHODS, n) == false) {
		print "Undefined property '", (PrintObject) n, "'.";
		RuntimeError("");
		rfalse;
	}
	return CallClosure(TableGet_value, arg_count);
];

[ Invoke n arg_count  receiver;
	receiver = PeekValue(arg_count);

	if (IsObjType(PeekType(arg_count), receiver, OBJ_INSTANCE) == false) {
		RuntimeError("Only instances have methods.");
		rfalse;
	}

	if (TableGet(receiver-->OBJINSTANCE_FIELDS, n)) {
		vm.stack-->((vm.stack_top - arg_count - 1) * VALUE_SIZE) = TableGet_type;
		vm.stack-->((vm.stack_top - arg_count - 1) * VALUE_SIZE + 1) = TableGet_value;
		return CallValue(TableGet_type, TableGet_value, arg_count);
	}

	return InvokeFromClass(receiver-->OBJINSTANCE_CLASS, n, arg_count);
];

[ BindMethod klass n  bound;
	if (TableGet(klass-->OBJCLASS_METHODS, n) == 0) {
		print "Undefined property '", (PrintObject) n, "'.";
		RuntimeError("");
		rfalse;
	}

	bound = NewBoundMethod(PeekValue(), TableGet_value);
	Pop();
	Push(VAL_OBJ, bound);
	rtrue;
];

[ CaptureUpvalue local  prev_upvalue uv created_upvalue;
	prev_upvalue = 0;
	uv = vm.open_upvalues;
	while (uv ~= 0 && uv-->OBJUPVALUE_LOCATION > local) {
		prev_upvalue = uv;
		uv = uv-->OBJUPVALUE_NEXT;
	}

	if (uv ~= 0 && uv-->OBJUPVALUE_LOCATION == local) {
		return uv;
	}

	created_upvalue = NewUpvalue(local);
	created_upvalue-->OBJUPVALUE_NEXT = uv;

	if (prev_upvalue == 0) {
		vm.open_upvalues = created_upvalue;
	} else {
		prev_upvalue-->OBJUPVALUE_NEXT = created_upvalue;
	}

	return created_upvalue;
];

! `last_uv` is an address! (Pointing a location on the stack.)
[ CloseUpvalues last_uv  uv;
	while (vm.open_upvalues ~= 0 && vm.open_upvalues-->OBJUPVALUE_LOCATION >= last_uv) {
		uv = vm.open_upvalues;
		! The type.
		uv-->OBJUPVALUE_CLOSED_TYPE = uv-->OBJUPVALUE_LOCATION-->0;
		! The value.
		uv-->OBJUPVALUE_CLOSED_VALUE = uv-->OBJUPVALUE_LOCATION-->1;
		uv-->OBJUPVALUE_LOCATION = uv + OBJUPVALUE_CLOSED_TYPE * WORDSIZE;
		vm.open_upvalues = uv-->OBJUPVALUE_NEXT;
	}
];

[ DefineMethod n  m klass;
	m = PeekValue(0);
	klass = PeekValue(1);
	TableSet(klass-->OBJCLASS_METHODS, n, OBJ_CLOSURE, m);
	Pop();
];

[ IsFalsey val_type value;
	return val_type == VAL_NIL || (val_type == VAL_BOOL && value == false);
];

[ Concatenate a b  new_length new_chars x y z;
	b = PeekValue(0);
	a = PeekValue(1);

	new_length = a-->OBJSTRING_LENGTH + b-->OBJSTRING_LENGTH;
	new_chars = Allocate(WORDSIZE, new_length + 1);

	x = a-->OBJSTRING_LENGTH * WORDSIZE;
	y = a-->OBJSTRING_CHARS;
	@mcopy x y new_chars;
	x = b-->OBJSTRING_LENGTH * WORDSIZE;
	y = b-->OBJSTRING_CHARS;
	z = new_chars + a-->OBJSTRING_LENGTH * WORDSIZE;
	@mcopy x y z;
	new_chars-->new_length = 0;
	Pop();
	Pop();
	Push(VAL_OBJ, TakeString(new_chars, new_length));
];

[ ReadByte  frame;
	frame = vm.frames-->(vm.frame_count - 1);
	return frame.closure-->OBJCLOSURE_FUNCTION-->OBJFUNCTION_CHUNK-->CHUNK_CODE->(frame.ip++);
];

! Returns the *address* of the value. So `returned_value-->0` is its type, and `returned_value-->1` is the actual value.
[ ReadConstant  frame;
	frame = vm.frames-->(vm.frame_count - 1);
	return frame.closure-->OBJCLOSURE_FUNCTION-->OBJFUNCTION_CHUNK-->CHUNK_CONSTANTS-->VALUE_ARRAY_VALUES + VALUE_SIZE * WORDSIZE * ReadByte();
];

[ ReadShort  frame a b;
	frame = vm.frames-->(vm.frame_count - 1);
	frame.ip = frame.ip + 2;
	a = frame.closure-->OBJCLOSURE_FUNCTION-->OBJFUNCTION_CHUNK-->CHUNK_CODE->(frame.ip - 2);
	@shiftl a 8 a;
	b = frame.closure-->OBJCLOSURE_FUNCTION-->OBJFUNCTION_CHUNK-->CHUNK_CODE->(frame.ip - 1);
	return a | b;
];

[ EnsureNumberOperands;
	if (PeekType(0) ~= VAL_NUMBER || PeekType(1) ~= VAL_NUMBER) {
		RuntimeError("Operands must be numbers.");
		rfalse;
	}
	rtrue;
];

[ Run  frame instruction a b c d e;
	frame = vm.frames-->(vm.frame_count - 1);
	while (true) {
		#Ifdef INFOLOX_TRACE_EXECUTION;
		spaces 10;
		for (a=0 : a<vm.stack_top : a++) {
			print "[ ";
			PrintValue(
				vm.stack-->(VALUE_SIZE * a),
				vm.stack-->(VALUE_SIZE * a + 1)
			);
			print " ]";
		}
		new_line;
		DisassembleInstruction(frame.closure-->OBJCLOSURE_FUNCTION-->OBJFUNCTION_CHUNK, frame.ip);
		#Endif;

		switch (instruction = ReadByte()) {
			OP_CONSTANT:
				a = ReadConstant();
				Push(a-->0, a-->1);
			OP_NIL: Push(VAL_NIL, 0);
			OP_TRUE: Push(VAL_BOOL, true);
			OP_FALSE: Push(VAL_BOOL, false);
			OP_POP: Pop();
			OP_GET_LOCAL:
				a = ReadByte();
				Push(
					frame.slots-->(a * VALUE_SIZE),
					frame.slots-->(a * VALUE_SIZE + 1)
				);
			OP_SET_LOCAL:
				a = ReadByte();
				frame.slots-->(a * VALUE_SIZE) = PeekType();
				frame.slots-->(a * VALUE_SIZE + 1) = PeekValue();
			OP_GET_GLOBAL:
				a = ReadConstant()-->1;
				if ((~~TableGet(vm.globals, a))) {
					print "Undefined variable '", (PrintObject) a, "'.";
					RuntimeError("");
					return INTERPRET_RUNTIME_ERROR;
				}
				Push(TableGet_type, TableGet_value);
			OP_DEFINE_GLOBAL:
				a = ReadConstant()-->1; ! Get the value of the constant.
				TableSet(vm.globals, a, PeekType(), PeekValue());
				Pop();
			OP_SET_GLOBAL:
				a = ReadConstant()-->1;
				if (TableSet(vm.globals, a, PeekType(), PeekValue())) {
					TableDelete(vm.globals, a);
					print "Undefined variable '", (PrintObject) a, "'.";
					RuntimeError("");
					return INTERPRET_RUNTIME_ERROR;
				}
			OP_GET_UPVALUE:
				a = ReadByte();
				b = frame.closure-->OBJCLOSURE_UPVALUES-->a-->OBJUPVALUE_LOCATION;
				Push(b-->0, b-->1);
			OP_SET_UPVALUE:
				a = ReadByte();
				b = frame.closure-->OBJCLOSURE_UPVALUES-->a-->OBJUPVALUE_LOCATION;
				b-->0 = PeekType();
				b-->1 = PeekValue();
			OP_GET_PROPERTY:
				if (IsObjType(PeekType(), PeekValue(), OBJ_INSTANCE) == false) {
					RuntimeError("Only instances have properties.");
					return INTERPRET_RUNTIME_ERROR;
				}

				a = PeekValue(); ! Instance.
				b = ReadConstant()-->1; ! Field.

				if (TableGet(a-->OBJINSTANCE_FIELDS, b)) {
					Pop(); ! Instance.
					Push(TableGet_type, TableGet_value);
				} else if (BindMethod(a-->OBJINSTANCE_CLASS, b) == false) {
					return INTERPRET_RUNTIME_ERROR;
				}
			OP_SET_PROPERTY:
				if (IsObjType(PeekType(1), PeekValue(1), OBJ_INSTANCE) == false) {
					RuntimeError("Only instances have fields.");
					return INTERPRET_RUNTIME_ERROR;
				}
				a = PeekValue(1); ! Instance.
				TableSet(a-->OBJINSTANCE_FIELDS, ReadConstant()-->1, PeekType(), PeekValue());
				b = PeekType(); ! Value type.
				c = Pop(); ! Value.
				Pop();
				Push(b, c);
			OP_GET_SUPER:
				a = ReadConstant()-->1; ! name.
				b = Pop(); ! superclass.

				if (BindMethod(b, a) == false) {
					return INTERPRET_RUNTIME_ERROR;
				}
			OP_EQUAL:
				c = PeekType();
				d = Pop();
				a = PeekType();
				b = Pop();
				Push(VAL_BOOL, ValuesEqual(a, b, c, d));
			OP_GREATER:
				if (~~EnsureNumberOperands()) return INTERPRET_RUNTIME_ERROR;
				b = Pop();
				a = Pop();
				@jfgt a b ?IsGreater;
				c = false;
				jump GreaterResult;
				.IsGreater;
				c = true;
				.GreaterResult;
				Push(VAL_BOOL, c);
			OP_LESS:
				if (~~EnsureNumberOperands()) return INTERPRET_RUNTIME_ERROR;
				b = Pop();
				a = Pop();
				@jflt a b ?IsLess;
				c = false;
				jump LessResult;
				.IsLess;
				c = true;
				.LessResult;
				Push(VAL_BOOL, c);
			OP_ADD:
				if (
					IsObjType(PeekType(0), PeekValue(0), OBJ_STRING)
					&& IsObjType(PeekType(1), PeekValue(1), OBJ_STRING)
				) {
					Concatenate();
				} else if (PeekType(0) == VAL_NUMBER && PeekType(1) == VAL_NUMBER) {
					b = Pop();
					a = Pop();
					@fadd a b a;
					Push(VAL_NUMBER, a);
				} else {
					RuntimeError("Operands must be two numbers or two strings.");
					return INTERPRET_RUNTIME_ERROR;
				}
			OP_SUBTRACT:
				if (~~EnsureNumberOperands()) return INTERPRET_RUNTIME_ERROR;
				b = Pop();
				a = Pop();
				@fsub a b a;
				Push(VAL_NUMBER, a);
			OP_MULTIPLY:
				if (~~EnsureNumberOperands()) return INTERPRET_RUNTIME_ERROR;
				b = Pop();
				a = Pop();
				@fmul a b a;
				Push(VAL_NUMBER, a);
			OP_DIVIDE:
				if (~~EnsureNumberOperands()) return INTERPRET_RUNTIME_ERROR;
				b = Pop();
				a = Pop();
				@fdiv a b a;
				Push(VAL_NUMBER, a);
			OP_NOT:
				a = PeekType();
				Push(VAL_BOOL, IsFalsey(a, Pop()));
			OP_NEGATE:
				if (PeekType() ~= VAL_NUMBER) {
					RuntimeError("Operand must be a number.");
					return INTERPRET_RUNTIME_ERROR;
				}
				a = Pop();
				! Glulx has no `@fneg` opcode, so we flip the sign bit ourselves.
				@bitxor a $80000000 a;
				Push(VAL_NUMBER, a);
			OP_PRINT:
				a = PeekType();
				PrintValue(a, Pop());
				new_line;
			OP_JUMP:
				a = ReadShort();
				frame.ip = frame.ip + a;
			OP_JUMP_IF_FALSE:
				a = ReadShort();
				if (IsFalsey(PeekType(), PeekValue())) frame.ip = frame.ip + a;
			OP_LOOP:
				a = ReadShort();
				frame.ip = frame.ip - a;
			OP_CALL:
				a = ReadByte();
				if ((~~CallValue(PeekType(a), PeekValue(a), a))) {
					return INTERPRET_RUNTIME_ERROR;
				}
				frame = vm.frames-->(vm.frame_count - 1);
			OP_INVOKE:
				a = ReadConstant()-->1; ! Method name.
				b = ReadByte(); ! Arg count.
				if (Invoke(a, b) == false) {
					return INTERPRET_RUNTIME_ERROR;
				}
				frame = vm.frames-->(vm.frame_count - 1);
			OP_SUPER_INVOKE:
				a = ReadConstant()-->1; ! Method name.
				b = ReadByte(); ! Arg count.
				c = Pop(); ! Superclass.
				if (InvokeFromClass(c, a, b) == false) {
					return INTERPRET_RUNTIME_ERROR;
				}
				frame = vm.frames-->(vm.frame_count - 1);
			OP_CLOSURE:
				! `a` is the function.
				! `b` is the closure.
				! `c` is the loop index.
				! `d` is the upvalue `is_local`.
				! `e` is the upvalue `index`.
				a = ReadConstant()-->1;
				b = NewClosure(a);
				Push(VAL_OBJ, b);
				for (c=0 : c<b-->OBJCLOSURE_UPVALUE_COUNT : c++) {
					d = ReadByte();
					e = ReadByte();
					if (d) {
						b-->OBJCLOSURE_UPVALUES-->c = CaptureUpvalue(frame.slots + e * VALUE_SIZE * WORDSIZE);
					} else {
						b-->OBJCLOSURE_UPVALUES-->c = frame.closure-->OBJCLOSURE_UPVALUES-->c;
					}
				}
			OP_CLOSE_UPVALUE:
				CloseUpvalues(vm.stack + (vm.stack_top - 1) * VALUE_SIZE * WORDSIZE);
				Pop();
			OP_RETURN:
				a = PeekType();
				b = Pop();
				CloseUpvalues(frame.slots);
				vm.frame_count--;
				if (vm.frame_count == 0) {
					Pop();
					return INTERPRET_OK;
				}

				! Since `vm.stack_top` is a value-index but `frame.slots` is an address, we have to compute its value-index.
				vm.stack_top = (frame.slots - vm.stack) / (VALUE_SIZE * WORDSIZE);
				Push(a, b);
				frame = vm.frames-->(vm.frame_count - 1);
			OP_CLASS:
				Push(VAL_OBJ, NewClass(ReadConstant()-->1));
			OP_INHERIT:
				a = PeekValue(1); ! Superclass.
				if (IsObjType(PeekType(1), a, OBJ_CLASS) == false) {
					RuntimeError("Superclass must be a class.");
					return INTERPRET_RUNTIME_ERROR;
				}

				b = PeekValue(0); ! Subclass.
				TableAddAll(a-->OBJCLASS_METHODS, b-->OBJCLASS_METHODS);
				Pop(); ! Subclass.
			OP_METHOD:
				DefineMethod(ReadConstant()-->1);
			default:
				print "Runtime error: unknown opcode^";
				return INTERPRET_RUNTIME_ERROR;
		}
	}
];

[ Interpret source  f c result;
	f = Compile(source);
	FreeTokens(); ! Free the tokens used by the scanner.
	if (f == 0) return INTERPRET_COMPILE_ERROR;

	Push(VAL_OBJ, f);
	c = NewClosure(f);
	Pop();
	Push(VAL_OBJ, c);
	CallClosure(c, 0);

	result = Run();

	return result;
];
