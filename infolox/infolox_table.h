! We store the table maximum load as a numerator and a denominator.
! It's easier than using a float.
! (Although we will overflow when multipling by the numerator for very big tables.)
Constant TABLE_MAX_LOAD_NUM = 3;
Constant TABLE_MAX_LOAD_DEN = 4;

! The indices of the elements of a table entry.
Constant ENTRY_KEY = 0; ! Address of ObjString used as the key
Constant ENTRY_TYPE = 1; ! Type of the contained value
Constant ENTRY_VALUE = 2; ! The value itself
! The size of a table entry, in words.
Constant ENTRY_SIZE = 3;

Constant TABLE_COUNT = 0; ! Number
Constant TABLE_CAPACITY = 1; ! Number
Constant TABLE_ENTRIES = 2; ! (Number, Number, Number)[]
Constant TABLE_SIZE = 3;

[ NewTable  x;
	x = TABLE_SIZE * WORDSIZE;
	@malloc x x;
	if (x == 0) InfoloxFatalError("NewTable", "couldn't allocate memory");
	InitTable(x);
	return x;
];

[ InitTable table;
	table-->TABLE_COUNT = 0;
	table-->TABLE_CAPACITY = 0;
	table-->TABLE_ENTRIES = 0;
];

[ FreeTable table;
	FreeArray(ENTRY_SIZE * WORDSIZE, table-->TABLE_ENTRIES, table-->TABLE_CAPACITY);
	@mfree table;
];

[ FindEntry entries capacity key  index entry tombstone;
	! We are obliged to use the optimisation described in the last chapter of _Crafting Interpreters_ (which works because the capacities are powers of 2), because the modulo operator is signed in Inform, and Glulx doesn't seem to have unsigned arithmetic.
	! (I mean, we could also implement a modulo using unsigned comparisons,  I guess.)
	! index = key-->OBJSTRING_HASH % capacity;
	index = key-->OBJSTRING_HASH & (capacity - 1);

	while (true) {
		! The address at which starts the entry.
		entry = entries + index * ENTRY_SIZE * WORDSIZE;
		if (entry-->ENTRY_KEY == 0) {
			if (entry-->ENTRY_TYPE == VAL_NIL) {
				! Empty entry.
				if (tombstone ~= 0) return tombstone;
				else return entry;
			} else {
				! We found a tombstone.
				if (tombstone == 0) tombstone = entry;
			}
		} else if (entry-->ENTRY_KEY == key) {
			! We found the key.
			return entry;
		}

		! index = (index + 1) % capacity;
		index = (index + 1) & (capacity - 1);
	}
];

! We use globals since it's easier than passing a pointer that will be filled with a value.
! They are filled if `TableGet` succeeds.
Global TableGet_type;
Global TableGet_value;
[ TableGet table key  entry;
	if (table-->TABLE_COUNT == 0) rfalse;

	entry = FindEntry(table-->TABLE_ENTRIES, table-->TABLE_CAPACITY, key);
	if (entry-->ENTRY_KEY == 0) rfalse;

	TableGet_type = entry-->ENTRY_TYPE;
	TableGet_value = entry-->ENTRY_VALUE;
	rtrue;
];

[ AdjustCapacity table new_capacity  new_entries i entry dest;
	new_entries = Allocate(ENTRY_SIZE * WORDSIZE, new_capacity);
	for (i=0 : i<new_capacity : i++) {
		! The address at which starts the entry.
		entry = new_entries + i * ENTRY_SIZE * WORDSIZE;
		entry-->ENTRY_KEY = 0;
		entry-->ENTRY_TYPE = VAL_NIL;
		entry-->ENTRY_VALUE = 0;
	}

	table-->TABLE_COUNT = 0;
	for (i=0 : i<table-->TABLE_CAPACITY : i++) {
		! The address at which starts the entry.
		entry = table-->TABLE_ENTRIES + i * ENTRY_SIZE * WORDSIZE;
		if (entry-->ENTRY_KEY == 0) continue;

		dest = FindEntry(new_entries, new_capacity, entry-->ENTRY_KEY);
		dest-->ENTRY_KEY = entry-->ENTRY_KEY;
		dest-->ENTRY_TYPE = entry-->ENTRY_TYPE;
		dest-->ENTRY_VALUE = entry-->ENTRY_VALUE;
		table-->TABLE_COUNT = table-->TABLE_COUNT + 1;
	}

	FreeArray(ENTRY_SIZE * WORDSIZE, table-->TABLE_ENTRIES, table-->TABLE_CAPACITY);
	table-->TABLE_ENTRIES = new_entries;
	table-->TABLE_CAPACITY = new_capacity;
];

[ TableSet table key val_type value  new_capacity entry is_new_key;
	if (table-->TABLE_COUNT + 1 > table-->TABLE_CAPACITY * TABLE_MAX_LOAD_NUM / TABLE_MAX_LOAD_DEN) {
		new_capacity = GrowCapacity(table-->TABLE_CAPACITY);
		AdjustCapacity(table, new_capacity);
	}

	entry = FindEntry(table-->TABLE_ENTRIES, table-->TABLE_CAPACITY, key);
	is_new_key = entry-->ENTRY_KEY == 0;
	if (is_new_key && entry-->ENTRY_TYPE == VAL_NIL) table-->TABLE_COUNT = table-->TABLE_COUNT + 1;

	entry-->ENTRY_KEY = key;
	entry-->ENTRY_TYPE = val_type;
	entry-->ENTRY_VALUE = value;
	return is_new_key;
];

[ TableDelete table key  entry;
	if (table-->TABLE_COUNT == 0) rfalse;

	! Find the entry.
	entry = FindEntry(table-->TABLE_ENTRIES, table-->TABLE_CAPACITY, key);
	if (entry-->ENTRY_KEY == 0) rfalse;

	! Place a tombstone in the entry.
	entry-->ENTRY_KEY = 0;
	entry-->ENTRY_TYPE = VAL_BOOL;
	entry-->ENTRY_VALUE = true;
	rtrue;
];

[ TableAddAll from to  i entry;
	for (i=0 : i<from-->TABLE_CAPACITY : i++) {
		! The address at which starts the entry.
		entry = from-->TABLE_ENTRIES + i * ENTRY_SIZE * WORDSIZE;
		if (entry-->ENTRY_KEY ~= 0) {
			TableSet(to, entry-->ENTRY_KEY, entry-->ENTRY_TYPE, entry-->ENTRY_VALUE);
		}
	}
];

[ TableFindString table c l hash  index entry;
	if (table-->TABLE_COUNT == 0) return 0;

	! index = hash % table-->TABLE_CAPACITY;
	index = hash & (table-->TABLE_CAPACITY - 1);
	while (true) {
		! The address at which starts the entry.
		entry = table-->TABLE_ENTRIES + index * ENTRY_SIZE * WORDSIZE;
		if (entry-->ENTRY_KEY == 0) {
			! Stop if we find an empty non-tombstone entry.
			if (entry-->ENTRY_TYPE == VAL_NIL) return 0;
		} else if (
			entry-->ENTRY_KEY-->OBJSTRING_LENGTH == l
			&& memcmp(entry-->ENTRY_KEY-->OBJSTRING_CHARS, c, l)
		) {
			return entry-->ENTRY_KEY;
		}

		! index = (index + 1) % table-->TABLE_CAPACITY;
		index = (index + 1) & (table-->TABLE_CAPACITY - 1);
	}
];

[ TableRemoveWhite table  i entry;
	for (i=0 : i<table-->TABLE_CAPACITY : i++) {
		entry = table-->TABLE_ENTRIES + i * ENTRY_SIZE * WORDSIZE;
		if (entry-->ENTRY_KEY ~= 0 && entry-->ENTRY_KEY-->OBJ_IS_MARKED == 0) {
			TableDelete(table, entry-->ENTRY_KEY);
		}
	}
];

[ MarkTable table  i entry;
	for (i=0 : i<table-->TABLE_CAPACITY : i++) {
		entry = table-->TABLE_ENTRIES + i * ENTRY_SIZE * WORDSIZE;
		MarkObject(entry-->ENTRY_KEY);
		MarkValue(entry-->ENTRY_TYPE, entry-->ENTRY_VALUE);
	}
];
