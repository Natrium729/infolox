Constant VAL_BOOL = 0;
Constant VAL_NIL = VAL_BOOL + 1;
Constant VAL_NUMBER = VAL_NIL + 1;
Constant VAL_OBJ = VAL_NUMBER + 1;

! The size of a value in words.
! (1 word for the type, 1 word for the value.)
! We could have used a single byte for the type, but it would make accessing the type of a value in an array more cumbersome.
Constant VALUE_SIZE = 2;

Constant VALUE_ARRAY_CAPACITY = 0;
Constant VALUE_ARRAY_COUNT = 1;
Constant VALUE_ARRAY_VALUES = 2;
Constant VALUE_ARRAY_SIZE = 3;

[ NewValueArray  x;
	x = VALUE_ARRAY_SIZE * WORDSIZE;
	@malloc x x;
	if (x == 0) InfoloxFatalError("NewValueArray", "couldn't allocate memory");
	InitValueArray(x);
	return x;
];

[ InitValueArray arr;
	arr-->VALUE_ARRAY_VALUES = 0;
	arr-->VALUE_ARRAY_CAPACITY = 0;
	arr-->VALUE_ARRAY_COUNT = 0;
];

[ FreeValueArray arr;
	FreeArray(1, arr-->VALUE_ARRAY_VALUES, arr-->VALUE_ARRAY_CAPACITY);
	@mfree arr;
];

[ WriteValueArray value_array val_type value  old_capacity;
	if (value_array-->VALUE_ARRAY_CAPACITY < value_array-->VALUE_ARRAY_COUNT + 1) {
		old_capacity = value_array-->VALUE_ARRAY_CAPACITY;
		value_array-->VALUE_ARRAY_CAPACITY = GrowCapacity(old_capacity);
		value_array-->VALUE_ARRAY_VALUES = GrowArray(VALUE_SIZE * WORDSIZE, value_array-->VALUE_ARRAY_VALUES, old_capacity, value_array-->VALUE_ARRAY_CAPACITY);
	}

	value_array-->VALUE_ARRAY_VALUES-->(VALUE_SIZE * value_array-->VALUE_ARRAY_COUNT) = val_type;
	value_array-->VALUE_ARRAY_VALUES-->(VALUE_SIZE * value_array-->VALUE_ARRAY_COUNT + 1) = value;
	value_array-->VALUE_ARRAY_COUNT = value_array-->VALUE_ARRAY_COUNT + 1;

];

[ PrintValue val_type value;
	switch (val_type) {
		VAL_BOOL: if (value) print "true"; else print "false";
		VAL_NIL: print "nil";
		VAL_NUMBER: print (Float) value;
		VAL_OBJ: PrintObject(value);
		default: InfoloxFatalError("PrintValue", "unreachable branch");
	}
];

[ ValuesEqual a_type a b_type b;
	if (a_type ~= b_type) rfalse;
	switch (a_type) {
		VAL_NIL: rtrue;
		VAL_NUMBER:
			! Necessary to take account of NaN.
			! (Since the Inform 6 `==` operator works on the values' underlying bits.)
			@jfeq a b $+0 ?FloatIsEq;
			rfalse;
			.FloatIsEq;
			rtrue;
		default: return a == b;
	}
];

! The Floating-point printing routines.
! They were taken from glulxercise.inf by Andrew Plotkin.

! Print a float. This uses exponential notation ("[-]N.NNNe[+-]NN") if
! the exponent is not between 6 and -4. If it is (that is, if the
! absolute value is near 1.0) then it uses decimal notation ("[-]NNN.NNNNN").
! The precision is the number of digits after the decimal point
! (at least one, no more than eight). The default is five, because
! beyond that rounding errors creep in, and even exactly-represented
! float values are printed with trailing fudgy digits.
[ Float val prec   pval;
	pval = val & $7FFFFFFF;

	@jz pval ?UseFloatDec;
	@jfge pval $49742400 ?UseFloatExp; ! 1000000.0
	@jflt pval $38D1B717 ?UseFloatExp; ! 0.0001

	.UseFloatDec;
	return FloatDec(val, prec);
	.UseFloatExp;
	return FloatExp(val, prec);
];

Array PowersOfTen --> 1 10 100 1000 10000 100000 1000000 10000000 100000000 1000000000;

! Print a float in exponential notation: "[-]N.NNNe[+-]NN".
! The precision is the number of digits after the decimal point
! (at least one, no more than eight). The default is five, because
! beyond that rounding errors creep in, and even exactly-represented
! float values are printed with trailing fudgy digits.
[ FloatExp val prec   log10val expo fexpo idig ix pow10;
	if (prec == 0)
		prec = 5;
	if (prec > 8)
		prec = 8;
	pow10 = PowersOfTen --> prec;

	! Knock off the sign bit first.
	if (val & $80000000) {
		@streamchar '-';
		val = val & $7FFFFFFF;
	}

	@jisnan val ?IsNan;
	@jisinf val ?IsInf;

	if (val == $0) {
		expo = 0;
		idig = 0;
		jump DoPrint;
	}

	! Take as an example val=123.5, with precision=6. The desired
	! result is "1.23000e+02".

	@log val sp;
	@fdiv sp $40135D8E log10val; ! $40135D8E is log(10)
	@floor log10val fexpo;
	@ftonumn fexpo expo;
	! expo is now the exponent (as an integer). For our example, expo=2.

	@fsub log10val fexpo sp;
	@numtof prec sp;
	@fadd sp sp sp;
	@fmul sp $40135D8E sp;
	@exp sp sp;
	! The stack value is now exp((log10val - fexpo + prec) * log(10)).
	! We've shifted the decimal point left by expo digits (so that
	! it's after the first nonzero digit), and then right by prec
	! digits. In our example, that would be 1235000.0.
	@ftonumn sp idig;
	! Round to an integer, and we have 1235000. Notice that this is
	! exactly the digits we want to print (if we stick a decimal point
	! after the first).

	.DoPrint;

	if (idig >= 10*pow10) {
		! Rounding errors have left us outside the decimal range of
		! [1.0, 10.0) where we should be. Adjust to the next higher
		! exponent.
		expo++;
		@div idig 10 idig;
	}

	for (ix=0 : ix<=prec : ix++) {
		@div idig pow10 sp;
		@mod sp 10 sp;
		@streamnum sp;
		if (ix == 0)
			@streamchar '.';
		@div pow10 10 pow10;
	}

	! Print the exponent. Convention is to use at least two digits.
	@streamchar 'e';
	if (expo < 0) {
		@streamchar '-';
		@neg expo expo;
	}
	else {
		@streamchar '+';
	}
	if (expo < 10)
		@streamchar '0';
	@streamnum expo;
	rtrue;

	.IsNan;
	@streamstr "NaN";
	rtrue;

	.IsInf;
	@streamstr "Inf";
	rtrue;
];

! Print a float in decimal notation: "[-]NNN.NNNNN".
! The precision is the number of digits after the decimal point
! (at least one, no more than eight). The default is five, because
! beyond that rounding errors creep in, and even exactly-represented
! float values are printed with trailing fudgy digits.
[ FloatDec val prec   log10val int fint extra0 frac idig ix pow10;
	if (prec == 0)
		prec = 5;
	if (prec > 8)
		prec = 8;
	pow10 = PowersOfTen --> prec;

	! Knock off the sign bit first.
	if (val & $80000000) {
		@streamchar '-';
		val = val & $7FFFFFFF;
	}

	@jisnan val ?IsNan;
	@jisinf val ?IsInf;

	! Take as an example val=123.5, with precision=6. The desired result
	! is "123.50000".

	extra0 = 0;
	@fmod val $3F800000 frac fint; ! $3F800000 is 1.0.
	@ftonumz fint int;
	! This converts the integer part of the value to an integer value;
	! in our example, 123.

	if (int == $7FFFFFFF) {
		! Looks like the integer part of the value is bigger than
		! we can store in an int variable. (It could be as large
		! as 3e+38.) We're going to have to use a log function to
		! reduce it by some number of factors of 10, and then pad
		! with zeroes.
		@log fint sp;
		@fdiv sp $40135D8E log10val; ! $40135D8E is log(10)
		@ftonumz log10val extra0;
		@sub extra0 8 extra0;
		! extra0 is the number of zeroes we'll be padding with.
		@numtof extra0 sp;
		@fsub log10val sp sp;
		@fmul sp $40135D8E sp;
		@exp sp sp;
		! The stack value is now exp((log10val - extra0) * log(10)).
		! We've shifted the decimal point far enough left to leave
		! about eight digits, which is all we can print as an integer.
		@ftonumz sp int;
	}

	! Print the integer part.
	@streamnum int;
	for (ix=0 : ix<extra0 : ix++)
		@streamchar '0';

	@streamchar '.';

	! Now we need to print the frac part, which is .5.

	@log frac sp;
	@fdiv sp $40135D8E log10val; ! $40135D8E is log(10)
	@numtof prec sp;
	@fadd log10val sp sp;
	@fmul sp $40135D8E sp;
	@exp sp sp;
	! The stack value is now exp((frac + prec) * log(10)).
	! We've shifted the decimal point right by prec
	! digits. In our example, that would be 50000.0.
	@ftonumn sp idig;
	! Round to an integer, and we have 50000. Notice that this is
	! exactly the (post-decimal-point) digits we want to print.

	.DoPrint;

	if (idig >= pow10) {
		! Rounding errors have left us outside the decimal range of
		! [0.0, 1.0) where we should be. I'm not sure this is possible,
		! actually, but we'll just adjust downward.
		idig = pow10 - 1;
	}

	@div pow10 10 pow10;
	for (ix=0 : ix<prec : ix++) {
		@div idig pow10 sp;
		@mod sp 10 sp;
		@streamnum sp;
		@div pow10 10 pow10;
	}
	rtrue;

	.IsNan;
	@streamstr "NaN";
	rtrue;

	.IsInf;
	@streamstr "Inf";
	rtrue;
];
